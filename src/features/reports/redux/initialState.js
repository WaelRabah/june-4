const initialState = {
  retrieveReportsBegin: false,
  retrieveReportsSuccess: false,
  retrieveReportsError: false,

  allReports: { data: [] },

  updatedReportBegin: false,
  updatedReportSuccess: false,
  updatedReportFailure: false,
  deleteReportsBegin: false,
  deleteReportsError: false,
  deleteReportsSuccess: false,

  createReportBegin: false,
  createReportSuccess: false,
  createReport_failure: false,
  createdReportId: null,

  retrieveAllTopicsDropDownBegin: true,
  retrieveAllTopicsDropDownFailure: false,
  retrieveAllTopicsDropDownSuccess: false,
  topicDropdown: [],
};

export default initialState;
