export const reports_table = [
  {
    id: '1',
    name: 'Monthly Update on KPI 1 Up in Singapore Towards Healthy Outcome (GUSTO)',
    charts: 'Sentiments Trend, Total Mentions, Emotions Trend',
    reports: '1st of every month',
    recipients: 'adeline.ang@email.com;benjamin.beh@email.com',
  },
  {
    id: '12',
    name: 'Growing Up in Singapore Towards Healthy Outcome (GUSTO)',
    charts: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit..',
    reports: 'A*STAR, Health, Healthcare, Singapore, Grow',
    recipients: 'adeline.ang@email.com;benjamin.beh@email.com',
  },
  {
    id: '13',
    name: 'Growing Up in Singapore Towards Healthy Outcome (GUSTO)',
    charts: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    reports: 'A*STAR, Health, Healthcare, Singapore, Grow',
    recipients: 'adeline.ang@email.com;benjamin.beh@email.com',
  },
  {
    id: '14',
    name: 'Growing Up in Singapore Towards Healthy Outcome (GUSTO)',
    charts: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    reports: 'A*STAR, Health, Healthcare, Singapore, Grow',
    recipients: 'adeline.ang@email.com;benjamin.beh@email.com',
  },
  {
    id: '15',
    name: 'Growing Up in Singapore Towards Healthy Outcome (GUSTO)',
    charts: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    reports: 'A*STAR, Health, Healthcare, Singapore, Grow',
    recipients: 'adeline.ang@email.com;benjamin.beh@email.com',
  },
  {
    id: '17',
    name: 'Growing Up in Singapore Towards Healthy Outcome (GUSTO)',
    charts: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    reports: 'A*STAR, Health, Healthcare, Singapore, Grow',
    recipients: 'adeline.ang@email.com;benjamin.beh@email.com',
  },
];
