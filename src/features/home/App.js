import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavigationBar, Header } from '../shared-components/';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoadingBar from 'react-redux-loading-bar';
import * as actions from './redux/actions';
import CategorySelectionModal from './components/CategorySelectionModal';
import ChangePassword from './components/ChangePassword';
import { ToastContainer, toast, Zoom } from 'react-toastify';
import qs from 'qs';
import 'react-toastify/dist/ReactToastify.css';
import ReactGA from 'react-ga'

class App extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  static defaultProps = {
    children: '',
  };

  state = {
    isCategorySelectionModal: false,
    allCategories: [],
    topicDropdown: [],
    allSources: [],
    updateTopicsDropdown: false,
    selectedCategoryId: null,
    isChangePassword: false,
  };

  updateSelectedTopic = id => {
    const { headerTopicData } = this.props.App;
    const { updateSelectedTopic } = this.props.actions;
    this.setState({ isLoading: true, selectedCategoryId: id }, () => {
      ReactGA.event(
        {
          category : 'Topic' ,
          action : 'Topic '+id+ ' was selected'
        }
      )
      updateSelectedTopic({
        updatedTopic: this.state.selectedCategoryId,
        prevTopic: headerTopicData,
      });
    });
  };

  setActiveTopic = topicStatus => {
    const { updateTopicsDropdown } = this.props.actions;
    const { headerTopicData } = this.props.App;
    this.setState({ updateTopicsDropdown: true }, () => {
      updateTopicsDropdown({ updatedTopic: topicStatus, prevTopic: headerTopicData });
    });
  };

  UNSAFE_componentWillReceiveProps(newProps) {
    const {
      updateTopicsdropdownSuccess,
      updateSelectedTopicSuccess,
      updateSelectedTopicFailure,
      allCategories,
    } = newProps.App;
    if (updateTopicsdropdownSuccess && this.state.updateTopicsDropdown) {
      toast.success('Selected topic has been changed!');
      this.setState({ updateTopicsDropdown: false }, () => {
        this.props.actions.retrieveActiveTopic();
        this.props.actions.retrieveTopicsDropdown();
        this.props.actions.retrieveActiveTopic();
      });
    }

    if (updateSelectedTopicSuccess && this.state.selectedCategoryId) {
      const {
        retrieveTopicsDropdown,
        retrieveAllCategories,
        retrieveActiveTopic,
      } = this.props.actions;
      toast.success('Selected topic has been changed!');
      this.setState(
        { selectedCategoryId: null, isCategorySelectionModal: false, isLoading: false },
        () => {
          const parsed = qs.parse(window.location.search,  { ignoreQueryPrefix: true });
          if (parsed.topicId && parsed.topicName) {
            this.props.history.push('/dashboard');
          }
          else {
          retrieveActiveTopic();
          retrieveAllCategories();
          retrieveTopicsDropdown();
          }
        },
      );
    }
    if (updateSelectedTopicFailure && this.state.selectedCategoryId) {
      toast.error('Selected topic not has been changed!');
      this.setState({
        selectedCategoryId: null,
        isCategorySelectionModal: false,
        isLoading: false,
      });
    }

    if (JSON.stringify(this.state.allCategories) !== JSON.stringify(allCategories)) {
      this.setState({
        allCategories,
      });
    }
  }

  setActiveTab = tab => {
    this.props.actions.toggleTabs(tab);
  };


  getLoginRedirectUrl = () => {
    const pageToRedictTo = '/login';
    if (window.location.search.length > 0) {
      return pageToRedictTo + window.location.search;
    }
    else {
      return pageToRedictTo;
    }
  }

  componentDidMount() {
    const { cookies } = this.props;
    
    if (this.props.location.pathname === '/' || !cookies.cookies.userMeta) {
      ReactGA.pageview(this.getLoginRedirectUrl())
      this.props.history.push(this.getLoginRedirectUrl());
      return false;
    }
    const {
      retrieveActiveTopic,
      retrieveTopicsDropdown,
      retrieveSourcesFilter,
    } = this.props.actions;
    retrieveSourcesFilter();
    retrieveActiveTopic();
    retrieveTopicsDropdown();
    
    const parsed = qs.parse(window.location.search,  { ignoreQueryPrefix: true });
    if (parsed.topicId && parsed.topicName) {
      let newTopicInfo = {status: false, topicId: parsed.topicId, topicName: parsed.topicName, topicStatus: true};
      this.updateSelectedTopic(newTopicInfo);
    }
    
  }
  
  render() {
    const { allCategories, isCategorySelectionModal, isLoading, isChangePassword } = this.state;
    return (
      <div className="home-app">
        <LoadingBar
          style={{ backgroundColor: '#2699fb', height: '5px', position: 'fixed', zIndex: '99' }}
        />
        <NavigationBar />
        <Header
          cookies={this.props.cookies}
          props={this.props}
          setCategorySelectionModalActive={e =>
            this.setState({ isCategorySelectionModal: e }, () => {
              this.props.actions.retrieveAllCategories();
            })
          }
          setActiveTopic={topicId => this.setActiveTopic(topicId)}
          setActiveTab={tab => this.setActiveTab(tab)}
          setIsChangePassword={() => this.setState({ isChangePassword: true })}
        />

        <div className="page-container">{this.props.children}</div>
        {isCategorySelectionModal && (
          <CategorySelectionModal
            show={isCategorySelectionModal}
            availableCategories={[...allCategories]}
            isLoading={isLoading}
            setCategorySelectionModalActive={e => this.setState({ isCategorySelectionModal: e })}
            updateSelectedTopic={e => this.updateSelectedTopic(e)}
          />
        )}
        {isChangePassword && (
          <ChangePassword
            cookies = {this.props.cookies}
            show={isChangePassword}
            setIsChangePassword={e => this.setState({ isChangePassword: e })}
          />
        )}
        <ToastContainer
          position="top-center"
          autoClose={3000}
          hideProgressBar
          newestOnTop
          closeOnClick
          transition={Zoom}
          rtl={false}
          pauseOnVisibilityChange
          draggable={false}
          pauseOnHover={false}
        />
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    App: state.home,
    cookies: ownProps.cookies,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
