import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Modal } from '../../shared-components';
import ReactGA from 'react-ga'

class CategorySelectionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCategories: [],
      selectedTopicId: {},
    };
  }

  componentDidMount() {
    if (this.props.show) {
      document.body.classList.add('modal-open');
    }
  }
  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      newProps.availableCategories.length &&
      JSON.stringify(newProps.availableCategories) !== JSON.stringify(this.state.allCategories)
    ) {
      this.setState({ allCategories: JSON.parse(JSON.stringify(newProps.availableCategories)) });
    }
  }

  componentWillUnmount() {
    document.body.classList.remove('modal-open');
  }

  setActiveTopic = (topIndex, index) => {
    const { allCategories } = this.state;
    const updatedCategories = allCategories.map(category => {
      category.topics.map(topic => {
        topic['topicStatus'] = false;
        return topic;
      });
      return category;
    });
    
    ReactGA.event(
      {
        category : 'Topic' ,
        action : 'Topic with topic_name='+updatedCategories[topIndex]['topics'][index]['topicName']+' of category '+updatedCategories[topIndex]['categoryName']+' was selected'
      }
    )
    updatedCategories[topIndex]['topics'][index]['topicStatus'] = !updatedCategories[topIndex][
      'topics'
    ][index]['topicStatus'];
    
    this.setState({
      allCategories: updatedCategories,
      selectedTopicId: updatedCategories[topIndex]['topics'][index],
    });
  };

  updateTopic = () => {
    const { updateSelectedTopic } = this.props;
    const { selectedTopicId } = this.state;
    ReactGA.event(
      {
        category : 'Topic' ,
        action : 'The selected Topic with topic_id='+selectedTopicId+' was updated'
      }
    )
    updateSelectedTopic(selectedTopicId);
  };

  render() {
    const { setCategorySelectionModalActive, show, isLoading } = this.props;
    const { allCategories } = this.state;
    return (
      <Modal handleClick={() => setCategorySelectionModalActive(false)}>
        <div className={`modal category-selector ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Select a Category or Topic</h4>
                <i
                  className="la la-close modal-close"
                  onClick={() => setCategorySelectionModalActive(false)}
                />
              </div>
              <div className="modal-body">
                {allCategories &&
                  allCategories.map((category, categoryIndex) => (
                    <div className="col-12 list-container" key={categoryIndex}>
                      <div className="topic-list col-12">
                        <ul>
                          <li className="main-list">
                            <label>{category.categoryName}</label>
                          </li>
                          {category.topics.map((topic, index) => (
                            <li
                              className={`sub-list ${topic.topicStatus ? 'active' : ''}`}
                              key={index + 1}
                              onClick={() => this.setActiveTopic(categoryIndex, index)}
                            >
                              <label>{topic.topicName}</label>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  ))}
              </div>
              <div className="modal-footer">
                <div className="manage-topics">
                  <Link
                    to="/dashboard/topics"
                    className={`${isLoading ? 'disabled' : ''}`}
                    onClick={() => setCategorySelectionModalActive(false)}
                  >
                    <button className={`btn ${isLoading ? 'disabled' : ''}`}>MANAGE TOPICS</button>
                  </Link>
                  <button
                    className={`btn ${isLoading ? 'disabled' : ''}`}
                    onClick={() => setCategorySelectionModalActive(false)}
                  >
                    CANCEL
                  </button>
                  <button
                    className={`btn btn-submit ${isLoading ? 'disabled' : ''}`}
                    onClick={() => this.updateTopic()}
                  >
                    {isLoading ? (
                      <React.Fragment>
                        <i className="la la-spinner la-spin" /> SAVING..
                      </React.Fragment>
                    ) : (
                      'SAVE'
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default CategorySelectionModal;
