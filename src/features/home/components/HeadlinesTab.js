import React, { Component } from 'react';
import { Headline } from './Headline.js';
import PropTypes from 'prop-types';
import {
    Loader,
    NoTopic
} from '../../shared-components';
import { Link } from 'react-router-dom';

class HeadlinesTab extends Component {
    static propTypes = {
        headlines: PropTypes.object.isRequired,
        loadMoreHeadlines: PropTypes.func.isRequired,
        isHeadlinesLoading: PropTypes.bool.isRequired,
        isInitialLoadOfHeadlinesForThisTopic: PropTypes.bool.isRequired,
        cookies: PropTypes.object.isRequired,
        updateHeadlinesRelevant: PropTypes.func.isRequired,
        retrieveCategoriesBegin: PropTypes.bool.isRequired,
        topicId: PropTypes.oneOfType([
            PropTypes.array.isRequired,
            PropTypes.object.isRequired
        ])
    };

  constructor(props) {
    super(props);
    this.state = {
        headlines: {
            'data': [],
            count: 0
        },
        isInitialLoadOfHeadlinesForThisTopic: false,
        isHeadlinesLoading: false,
    };
  }

  componentDidMount() {
    this._setHeadlinesToStateIfNewHeadlines(this.props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this._setHeadlinesToStateIfNewHeadlines(nextProps);
  }

  setHeadlinesRelevant = headlineId => {
    const { updateHeadlinesRelevant } = this.props;
    updateHeadlinesRelevant(headlineId);
  };


  _setHeadlinesToStateIfNewHeadlines(nextProps) {
    let updatedState = {};
    if (nextProps.isInitialLoadOfHeadlinesForThisTopic != this.state.isInitialLoadOfHeadlinesForThisTopic) {
      updatedState['isInitialLoadOfHeadlinesForThisTopic'] = nextProps.isInitialLoadOfHeadlinesForThisTopic;
    }
    if (
      Object.keys(nextProps.headlines).length &&
      JSON.stringify(this.state.headlines) !== JSON.stringify(nextProps.headlines)
    ) {
      if (Array.isArray(nextProps.headlines.data)) {
        updatedState['headlines'] = nextProps.headlines;
      }
    }
    if (nextProps.isHeadlinesLoading !== this.state.isHeadlinesLoading) {
      updatedState['isHeadlinesLoading'] = nextProps.isHeadlinesLoading;
    }

    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  render() {
    const { topicId } = this.props;
    const { headlines, isHeadlinesLoading, isInitialLoadOfHeadlinesForThisTopic } = this.state;
    return (
        <div className={`feed-comments`}>
        {topicId && topicId.length > 0 ? (
          <React.Fragment>
            {isInitialLoadOfHeadlinesForThisTopic && <Loader />}
            {!isInitialLoadOfHeadlinesForThisTopic && headlines.data.length > 0 &&
              headlines.data.map((headline, index) => (
                <Headline
                  cookies={this.props.cookies.cookies}
                  userHeadline={headline}
                  key={index}
                  updateRelevance={e => this.setHeadlinesRelevant(e)}
                />
              ))}
            {!isHeadlinesLoading && !isInitialLoadOfHeadlinesForThisTopic && !headlines.data.length && (
              <div className="no-feed-comment-data">
                <NoTopic
                  title="No Articles available"
                  message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                  icons={['exclamation-circle']}
                >
                  <button className="btn-no-data" onClick={() => this.showContactUs()}>
                    CONTACT US
                  </button>
                </NoTopic>
              </div>
            )}
          </React.Fragment>
        ) : (
          <NoTopic
            title="It’s Empty"
            message="Add topics and the related comments will appear here."
            icons={['comments']}
          >
            <Link to="/dashboard/topics">
              <button className="btn-add-topic">ADD TOPIC</button>
            </Link>
          </NoTopic>
        )}
        {!isInitialLoadOfHeadlinesForThisTopic && headlines['count'] && headlines['count'] > headlines['data'].length && (
          <button
            type="button"
            className={`btn btn-loadmore ${isHeadlinesLoading ? 'disabled' : ''}`}
            onClick={() => this.props.loadMoreHeadlines()}
          >
            {isHeadlinesLoading ? (
              <React.Fragment>
                <i className="la la-spinner la-spin" /> Loading
              </React.Fragment>
            ) : (
              'LOAD MORE'
            )}
          </button>
        )}
      </div>
    );
  }
}

export default HeadlinesTab;
