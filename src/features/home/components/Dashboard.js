import React, { Component } from 'react';
import Filter from './Filter';
import Feeds from './Feeds';
import FeedComments from './feed-comments';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import ReactGA from 'react-ga';
import moment from 'moment';
import {
  Button,
  Calender,
  WorldMapChart,
  WordCloud,
  BarChart,
  PieChart,
  ColumnChart,
  SourcesPercentColumnChart,
  SpiderChart,
  AreaChart,
  LineChart,
  SentimentsChart,
  EmotionsChart,
  NoTopic,
  Loader,
  ContactUs
} from '../../shared-components';
import FeedDetails from './Feeds-Detail-Modal';
import AddChartModal from './AddChartModal';
import ShareReportModal from './ShareReportModal';
import { Link } from 'react-router-dom';
import qs from 'qs';
import TweetsTab from './TweetsTab';
import HeadlinesTab from './HeadlinesTab';

import { 
  TAB_NAME_COMMENTS,
  TAB_NAME_FEEDS,
  TAB_NAME_TWEETS,
  TAB_NAME_HEADLINES,
  SOURCE_NAME_TWITTER,
  SOURCE_NAME_ALL_NEWS,
  SOURCE_NAME_ALL_SOCIAL_MEDIA,
  SOURCE_TYPE_NEWS
} from '../redux/constants';
const Filters = [
  { value: 'all', name: 'all', checked: true },
  { value: 'Positive', name: 'positive', checked: true },
  { value: 'negative', name: 'negative', checked: true },
  { value: 'neutral', name: 'neutral', checked: true },
  { value: 'joy', name: 'joy', checked: true },
  { value: 'anger', name: 'anger', checked: true },
  { value: 'fear', name: 'fear', checked: true },
  { value: 'sadness', name: 'sadness', checked: true },
];
const feedModalItemCount = 10;
class Dashboard extends Component {
  constructor(props) {
    super(props);
    const {cookies} = props;
    this.state = {
      activeFeed: 'comments',
      isFilter: false,
      FilterItems: JSON.parse(JSON.stringify(Filters)),
      isLoader: false,
      isFeedModal: false,
      selectedFeedIndex: null,
      activeFeedCategory: 'positive',
      isAddChartActive: false,
      isCommentsActive: false,
      isShareModalActive: false,
      isCommentsLoading: false,
      isTweetsLoading: false,
      isHeadlinesLoading: false,
      isInitialLoadOfCommentsForThisTopic: false,
      isInitialLoadOfTweetsForThisTopic: false,
      isInitialLoadOfHeadlinesForThisTopic: false,
      showTweetsTabShortcutButton: false,
      showHeadlinesTabShortcutButton: false,
      dateRangeFilterValue: {
        fromDate: moment()
          .subtract(6, 'days')
          .format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      filterValue: '',
      sourcesFilter: [],
      fullSourcesFilter: [],
      isSendReport: false,
      reportMailinglist: [],
      isFilterInChart: false,
      isContactUs: false,
      feedMenu: ['comments', 'feeds','tweets','headlines'],
      comments: {
        'data': [],
        count: 0
      },
      feeds: {
        'data': [],
        count: 0
      },
      tweets: {
        'data': [],
        count: 0
      },
      headlines: {
        'data': [],
        count: 0
      }
    };
    document.addEventListener('mousedown', this.handleClickOutside);
    this.wrapperRef = React.createRef();
    this.chartContainer = React.createRef();
  }

  setCommentsRelevant = commentId => {
    const { updateCommentsRelevant } = this.props;
    updateCommentsRelevant(commentId);
  };

  setFeedsRelevant = feedId => {
    const { updateFeedsRelevant } = this.props;
    updateFeedsRelevant(feedId);
  };
  showContactUs = () => {
    this.setState(prevState => {
      return {
        isContactUs: !prevState.isContactUs,
      };
    });
  };
  handleCommentView = type => {
    ReactGA.event(
      {
        category : type ,
        action : `user with id  ${this.props.cookies.cookies.userMeta} is viewing the ${type} `
      }
    ) 
    
    
    
    this.setState(prevState => {
      return {
        activeFeed: type,
        isCommentsActive: !prevState.isCommentsActive,
      };
    });
  };

  updateAvailableChart = e => {
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.cookies.userMeta} added a chart ${JSON.stringify(e)}`
      }
    )
    this.props.updateSelectedCharts(e);
    this.setState({ isAddChartActive: false });
  };
  handleClickOutside = event => {
    if (this.wrapperRef.current && !this.wrapperRef.current.contains(event.target)) {
      this.setState({
        isFilter: false,
      });
    }
  };

  filterBySource = source => {
    if (source.length) {
      this.props.filter({ value: source, key: 'sources', sources: source });
      this.setState({ sourcesFilter: source }, () => {
        let filter = localStorage.getItem('dashboardFilter');
        filter =
          filter && filter.length
            ? { ...JSON.parse(filter), ...{ source: this.state.sourcesFilter } }
            : { source: this.state.sourcesFilter };
        localStorage.setItem('dashboardFilter', JSON.stringify(filter));
        ReactGA.event(
          {
            category : 'Sources_Filter' ,
            action : `user with id ${this.props.cookies.cookies.userMeta} filtered sources in dashboard by ${JSON.stringify(filter)}`
          }
        )
      });
    } else {
      let filter = localStorage.getItem('dashboardFilter');
      if (filter) {
        filter = JSON.parse(filter);
        if (filter.source) {
          delete filter.source;
        }
        localStorage.setItem('dashboardFilter', JSON.stringify(filter));
      }
    }
  };

  _hideTabsForUnselectedSources() {
    const isSelectedOnSourceTab              = this._getWhichSourcesForDashboardTabsAreSelectedOnSourcesTab();
    const isSelectedOnDashboardSourcesFilter = this._getWhichSourcesForDashboardTabsAreSelectedInDashboardSourcesFilter(isSelectedOnSourceTab);

    let showTwitter   = false;
    let showHeadlines = false;
    if (isSelectedOnSourceTab[TAB_NAME_TWEETS] && isSelectedOnDashboardSourcesFilter[TAB_NAME_TWEETS]) {
      showTwitter = true;
    }
    if (isSelectedOnSourceTab[TAB_NAME_HEADLINES] && isSelectedOnDashboardSourcesFilter[TAB_NAME_HEADLINES]) {
      showHeadlines = true;
    }

    if (showTwitter && showHeadlines) {
      this.setState({
        feedMenu: [TAB_NAME_COMMENTS,TAB_NAME_FEEDS,TAB_NAME_TWEETS,TAB_NAME_HEADLINES],
        showTweetsTabShortcutButton: true,
        showHeadlinesTabShortcutButton: true
      });
    }
    else if (showTwitter) {
      this.setState({
        feedMenu: [TAB_NAME_COMMENTS,TAB_NAME_FEEDS,TAB_NAME_TWEETS],
        showTweetsTabShortcutButton: true,
        showHeadlinesTabShortcutButton: false
      });
    }
    else if (showHeadlines) {
      this.setState({
        feedMenu: [TAB_NAME_COMMENTS,TAB_NAME_FEEDS,TAB_NAME_HEADLINES],
        showTweetsTabShortcutButton: false,
        showHeadlinesTabShortcutButton: true
      });
    }
    else {
      this.setState({
        feedMenu: [TAB_NAME_COMMENTS,TAB_NAME_FEEDS],
        showTweetsTabShortcutButton: false,
        showHeadlinesTabShortcutButton: false
      });
    }
  }

  _getListOfNewsSources() {
    let news_sources = [];
    for (let i=0;i<this.state.fullSourcesFilter.length;i++) {
      let source = this.state.fullSourcesFilter[i];
      if (source['sourceType'] === SOURCE_TYPE_NEWS) {
        news_sources.push(source['sourceName'])
      }
    }
    return news_sources;
  }

  _getWhichSourcesForDashboardTabsAreSelectedOnSourcesTab() {
    let isSelectedOnSourceTab = {};
    isSelectedOnSourceTab[TAB_NAME_TWEETS]    = false;
    isSelectedOnSourceTab[TAB_NAME_HEADLINES] = false;
    for (let i=0;i<this.state.fullSourcesFilter.length;i++) {
      let source = this.state.fullSourcesFilter[i];
      if (source['sourceName'] === SOURCE_NAME_TWITTER && source['selected'] === true) {
        isSelectedOnSourceTab[TAB_NAME_TWEETS] = true;
      }
      if (source['sourceType'] === SOURCE_TYPE_NEWS && source['selected'] === true) {
        isSelectedOnSourceTab[TAB_NAME_HEADLINES] = true;
      }
    }
    return isSelectedOnSourceTab;
  }

  _getWhichSourcesForDashboardTabsAreSelectedInDashboardSourcesFilter(isSelectedOnSourceTab) {
    const news_sources                                     = this._getListOfNewsSources();
    let isSelectedOnDashboardSourcesFilter                 = {};
    isSelectedOnDashboardSourcesFilter[TAB_NAME_TWEETS]    = false;
    isSelectedOnDashboardSourcesFilter[TAB_NAME_HEADLINES] = false;
    if (isSelectedOnSourceTab[TAB_NAME_TWEETS]) {
      if (this.state.sourcesFilter.length == 0 || this.state.sourcesFilter.includes(SOURCE_NAME_ALL_SOCIAL_MEDIA)|| this.state.sourcesFilter.includes(SOURCE_NAME_TWITTER) ) {
        isSelectedOnDashboardSourcesFilter[TAB_NAME_TWEETS] = true;
      }
    }
    if (isSelectedOnSourceTab[TAB_NAME_HEADLINES]) {
      if (this.state.sourcesFilter.length == 0 || this.state.sourcesFilter.includes(SOURCE_NAME_ALL_NEWS) ) {
        isSelectedOnDashboardSourcesFilter[TAB_NAME_HEADLINES] = true;
      }
      for (let i=0;i<news_sources.length;i++) {
        let news_source = news_sources[i];
        if (this.state.sourcesFilter.includes(news_source)) {
          isSelectedOnDashboardSourcesFilter[TAB_NAME_HEADLINES] = true;
        }
      }
    }
    return isSelectedOnDashboardSourcesFilter;
  }

  updateDimensions = () => {
    if (window.innerWidth <= 1024) {
      this.setState({ isCommentsActive: false });
    } 
  };

  componentDidMount() {
    const { cookies } = this.props;
    ReactGA.pageview('/dashboard/'+cookies.cookies.userMeta)  
    window.addEventListener('resize', this.updateDimensions);
    this.setState({ isCommentsActive: false });
    const parsed = qs.parse(window.location.search,  { ignoreQueryPrefix: true });
    let filter = localStorage.getItem('dashboardFilter');
    this._setFullSourcesFilterToStateIfNewSourcesFilter(this.props);
    if (parsed.fromDate && parsed.toDate) {
      let queryFilter = {
        fromDate: parsed['fromDate'],
        toDate: parsed['toDate']
      };
      this.setDateRangeFilter(queryFilter);
    }
    else if (filter) {
      filter = JSON.parse(filter);
      if (filter.dateRangeFilterValue) {
        this.setState({
          dateRangeFilterValue: filter.dateRangeFilterValue,
        });
      }
    }

    this._loadFilterFromLocalStorageIntoState();
    this._setSourcesFilterToStateIfNewSourcesFilter(this.props);
    this._updateCommentRelatedStateIfNeeded(this.props);
    this._setFeedsToStateIfNewFeeds(this.props);
    this._updateTweetRelatedStateIfNeeded(this.props);
    this._updateHeadlineRelatedStateIfNeeded(this.props);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
    window.removeEventListener('resize', this.updateDimensions);
  }

  handleFeedDetails = (sentiment, index) => {
    this.props.retrieveFeedDetails({
      sentiment,
      pageLimit: feedModalItemCount,
      pageId: 1,
      feedId: this.props.feeds.data[index].feedId,
    });
    this.setState({
      isFeedModal: true,
      activeFeedCategory: sentiment,
      selectedFeedIndex: index,
      selectedFeedId: this.props.feeds.data[index].feedId,
    });
  };
  retrieveselectedSentmentFeedDetails = sentiment => {
    const { selectedFeedId } = this.state;
    const queryParams = {
      sentiment,
      pageLimit: feedModalItemCount,
      pageId: 1,
      feedId: selectedFeedId,
    };
    this.setState(
      {
        activeFeedCategory: sentiment,
        feedModalCurrentPage: 1,
      },
      () => {
        this.props.retrieveFeedDetails(queryParams);
      },
    );
  };

  loadMoreFeedDetails = () => {
    const { feedModalCurrentPage, selectedFeedId } = this.state;
    const queryParams = {
      sentiment: this.state.activeFeedCategory,
      pageLimit: feedModalItemCount,
      pageId: Number(feedModalCurrentPage) + 1,
      feedId: selectedFeedId,
    };
    this.setState(
      {
        feedModalCurrentPage: Number(feedModalCurrentPage) + 1,
      },
      () => {
        this.props.loadMoreFeedDetails(queryParams);
      },
    );
  };
  handleFilter = event => {
    const { FilterItems } = this.state;
    const value = event.target.value;

    const updatedFilter = FilterItems.map(filterItem => {
      if (value === 'all') {
        
          if (event.target.checked)
         {
          ReactGA.event(
            {
              category : 'Filter' ,
              action : 'User with id' + this.props.cookies.cookies.userMeta+' filtered  according to all filters'
            }
          ) 
         }
         else 
         {
          ReactGA.event(
            {
              category : 'Filter' ,
              action : 'User with id ' + this.props.cookies.cookies.userMeta+' removed the '+value+' filter'
            }
          ) 
         }
        
       
        return {
          ...filterItem,
          checked: event.target.checked,
        };
      }
      
      if (filterItem.value === value)
      {
        if (event.target.checked)
        {
         ReactGA.event(
           {
             category : 'Filter' ,
             action : 'User with id ' + this.props.cookies.cookies.userMeta+' filtered  according to '+value
           }
         ) 
        } 
        else 
        {
         ReactGA.event(
           {
             category : 'Filter' ,
             action : 'User with id ' + this.props.cookies.cookies.userMeta+' removed the '+value+' filter'
           }
         ) 
        }  
        return {
          ...filterItem,
          checked: !filterItem.checked,
        };
       }
      
      
      return filterItem;
    
    });
    const selectedItems = updatedFilter.filter(filter => filter.checked && filter.value !== 'all');
    if (selectedItems.length === 7) {
      updatedFilter[0].checked = true;
    } else {
      updatedFilter[0].checked = false;
    }
    this.setState(
      {
        FilterItems: updatedFilter,
      },
      () => {
        let filter = '';
        if (value !== 'all') {
          filter = this.state.FilterItems.filter(item => item.checked);

          filter = filter.map(i => i.value);
          if (filter.indexOf('all') !== -1) filter.splice(filter.indexOf('all'), 1);
        }
        this.props.filter({ value: filter, key: 'sentiments' });
        this._saveFilterFromStateIntoLocalStorage();
      },
    );
  };

  _saveFilterFromStateIntoLocalStorage() {
    let dashboardFilter = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (! dashboardFilter) {
      dashboardFilter = {};
    }
    dashboardFilter.FilterItems = this.state.FilterItems;
    localStorage.setItem('dashboardFilter',JSON.stringify(dashboardFilter));
  }

  _loadFilterFromLocalStorageIntoState() {
    let dashboardFilter = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (dashboardFilter && dashboardFilter.FilterItems) {
      this.setState({FilterItems: dashboardFilter.FilterItems});
    }
  }

  loadMoreAction = () => {
    this.setState({ isLoader: true }, () => {
      setTimeout(() => {
        this.setState({ isLoader: false });
      }, 2000);
    });
  };

  resetChartFilter = () => {
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.userMeta} has reset his chart filter`
      }
    )
    this.setState(
      {
        isFilterInChart: false,
      },
      () => {
        this.props.resetFilterInChart();
      },
    );
  };

  filterBySentimentsFromChart = e => {
    this.setState({
      isFilterInChart: true,
    });
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.userMeta} filtered his charts by ${e.sentiment}`
      }
    )
    this.props.filter({ value: e.sentiment, key: 'sentiments', date: e.dateTime });
  };

  exportToPdf = () => {
    const el = this.chartContainer.current;
    const HTML_Width = el.offsetWidth;
    const HTML_Height = el.offsetHeight;
    const top_left_margin = 30;
    const PDF_Width = HTML_Width + top_left_margin * 2;
    const PDF_Height = PDF_Width * 1.5 + top_left_margin * 2;
    const canvas_image_width = HTML_Width;
    const canvas_image_height = HTML_Height;
    const totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
    const currentDate = moment().format('MM-DD-YYYY');
    const { isSendReport } = this.state;
    
    let topicName = this.props.headerTopicData.topicName || 'NO_TOPIC';
    ReactGA.event(
      {
        category : 'User' ,
        action : `User with id ${this.props.cookies.cookies.userMeta} downloaded the report for ${topicName} on ${currentDate}  as a pdf file `
      }
    ) 
    html2canvas(el, {
      allowTaint: true,
      removeContainer: false,
    }).then(function(canvas) {
      canvas.getContext('2d');
     
      var imgData = canvas.toDataURL('image/png', 1);
      var pdf = new jsPDF('', 'pt', [PDF_Height, PDF_Width]);
      pdf.setTextColor(102, 102, 102);
      pdf.setFillColor(247, 247, 247, 1);
      pdf.setFontSize(12);
      pdf.text(top_left_margin, 25, `Topic: ${topicName}`);
      pdf.text(canvas_image_width - 130, 25, `Downloaded on: ${currentDate}`);
      pdf.addImage(
        imgData,
        'JPG',
        top_left_margin,
        top_left_margin,
        canvas_image_width,
        canvas_image_height,
      );

      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(
          imgData,
          'JPG',
          top_left_margin,
          -(PDF_Height * i) + top_left_margin,
          canvas_image_width,
          canvas_image_height,
        );
      }

      pdf.save(`${topicName}_${currentDate}_report.pdf`);
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!nextProps.isShareReportSending) {
      this.setState({ isSendReport: false, isShareModalActive: false });
    }

    this._setFullSourcesFilterToStateIfNewSourcesFilter(nextProps);
    this._setSourcesFilterToStateIfNewSourcesFilter(nextProps);
    this._updateCommentRelatedStateIfNeeded(nextProps);
    this._setFeedsToStateIfNewFeeds(nextProps);
    this._updateTweetRelatedStateIfNeeded(nextProps);
    this._updateHeadlineRelatedStateIfNeeded(nextProps);
  }

  _setFullSourcesFilterToStateIfNewSourcesFilter(nextProps) {
    if (
      Object.keys(nextProps.sourcesFilter).length && 
      JSON.stringify(this.state.fullSourcesFilter !== JSON.stringify(nextProps.sourcesFilter))) {
        this.setState({fullSourcesFilter: nextProps.sourcesFilter}, () => {
          this._hideTabsForUnselectedSources();
        });
      }
  }

  _setSourcesFilterToStateIfNewSourcesFilter(nextProps) {
    if (
      Object.keys(nextProps.sources).length &&
      JSON.stringify(this.state.sourcesFilter) !== JSON.stringify(nextProps.sources)
    ) {
        this.setState({sourcesFilter: nextProps.sources}, () => {
          this._hideTabsForUnselectedSources();
        });
    }
  }

  _updateCommentRelatedStateIfNeeded(nextProps) {
    let updatedState = {};
    if (nextProps.isInitialLoadOfCommentsForThisTopic != this.state.isInitialLoadOfCommentsForThisTopic) {
      updatedState['isInitialLoadOfCommentsForThisTopic'] = nextProps.isInitialLoadOfCommentsForThisTopic;
    }
    if (nextProps.isCommentsLoading !== this.state.isCommentsLoading) {
      updatedState['isCommentsLoading'] = nextProps.isCommentsLoading;
    }
    if (this._newListOfCommentsReceivedFromProps(nextProps)) {
      updatedState['comments'] = nextProps.comments; 
    }

    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  _newListOfCommentsReceivedFromProps(nextProps) {
    if (Object.keys(nextProps.comments).length &&
        JSON.stringify(this.state.comments) !== JSON.stringify(nextProps.comments) && 
        Array.isArray(nextProps.comments.data)
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  _updateTweetRelatedStateIfNeeded(nextProps) {
    let updatedState = {};
    if (nextProps.isInitialLoadOfTweetsForThisTopic != this.state.isInitialLoadOfTweetsForThisTopic) {
      updatedState['isInitialLoadOfTweetsForThisTopic'] = nextProps.isInitialLoadOfTweetsForThisTopic;
    }
    if (nextProps.isTweetsLoading !== this.state.isTweetsLoading) {
      updatedState['isTweetsLoading'] = nextProps.isTweetsLoading;
    }
    if (this._newListOfTweetsReceivedFromProps(nextProps)) {
      updatedState['tweets'] = nextProps.tweets; 
    }

    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  _newListOfTweetsReceivedFromProps(nextProps) {
    if (
      Object.keys(nextProps.tweets).length &&
      JSON.stringify(this.state.tweets) !== JSON.stringify(nextProps.tweets) && 
      (Array.isArray(nextProps.tweets.data))
     ) {
        return true;
      }
      else {
        return false;
      }
  }

  _updateHeadlineRelatedStateIfNeeded(nextProps) {
    let updatedState = {};
    if (nextProps.isInitialLoadOfHeadlinesForThisTopic != this.state.isInitialLoadOfHeadlinesForThisTopic) {
      updatedState['isInitialLoadOfHeadlinesForThisTopic'] = nextProps.isInitialLoadOfHeadlinesForThisTopic;
    }
    if (nextProps.isHeadlinesLoading !== this.state.isHeadlinesLoading) {
      updatedState['isHeadlinesLoading'] = nextProps.isHeadlinesLoading;
    }
    if (this._newListOfHeadlinesReceivedFromProps(nextProps)) {
      updatedState['headlines'] = nextProps.headlines; 
    }

    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  _newListOfHeadlinesReceivedFromProps(nextProps) {
    if (Object.keys(nextProps.headlines).length &&
        JSON.stringify(this.state.headlines) !== JSON.stringify(nextProps.headlines) && 
        Array.isArray(nextProps.headlines.data)
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  _setFeedsToStateIfNewFeeds(nextProps) {
    if (
      Object.keys(nextProps.feeds).length &&
      JSON.stringify(this.state.feeds) !== JSON.stringify(nextProps.feeds)
    ) {
      if (Array.isArray(nextProps.feeds.data)) {
        let feeds = {
          'count': nextProps.feeds.count,
          'data': []
        };
        let topicId = nextProps.topicId[0];
        for (let i=0;i<nextProps.feeds.data.length;i++) {
          let feed = nextProps.feeds.data[i];
          if (feed.topicId == topicId) {
            feeds.data.push(feed);
          }
        }
        this.setState({
          feeds: feeds,
        });
      }
    }
  }

  sendReportEmail = () => {
    let topicName   = this.props.headerTopicData.topicName || 'NO_TOPIC';
    const {topicId} = this.props;
    const { 
      reportMailinglist,
      dateRangeFilterValue
    } = this.state;
    ReactGA.event(
      {
        category : 'User' ,
        action : `User with id ${this.props.cookies.cookies.userMeta} shared the report with ${reportMailinglist}`
      }
    ) 
    const formData = {
      email: reportMailinglist.join(','),
      topic_id: topicId[0],
      topic_name: topicName,
      from_date: dateRangeFilterValue.fromDate,
      to_date: dateRangeFilterValue.toDate
    };
    this.props.sendEmailReport(formData);
  };

  sendReport = emailList => {
    this.setState(
      {
        isSendReport: true,
        reportMailinglist: emailList,
      },
      () => {
        this.sendReportEmail();
      },
    );
  };

  setDateRangeFilter = e => {
    this.setState({ dateRangeFilterValue: e }, () => {
      let filter = localStorage.getItem('dashboardFilter');
      filter =
        filter && filter.length
          ? { ...JSON.parse(filter), ...{ dateRangeFilterValue: e } }
          : { dateRangeFilterValue: e };
      localStorage.setItem('dashboardFilter', JSON.stringify(filter));
    });
    this.props.filterFeedsCommentsByDate(e);
  };

  setShareReportModal = () => {
    this.setState({ isShareModalActive: true });
    this.props.retrieveShareReportMessage();
  };

  actionButtons = activeFeed => {
    const { FilterItems, isFilter } = this.state;
    const { comments, feeds } = this.props;
    return (
      <div className="feeds-action">
          <Button
            icon="download"
            label="CSV"
            onClick={() => {
              
              ReactGA.event(
                {
                  category : activeFeed ,
                  action : activeFeed + ' csv data was downloaded by user with id '+this.props.cookies.cookies.userMeta
                }
              )    
              this.props.getCsvData(activeFeed);
            }}
          />
          <div
            className={`input-container ${isFilter ? 'active' : ''}`}
            ref={this.wrapperRef}
          >
            <div
              className="toggle-btn"
              onClick={() =>
                this.setState(prevState => {
                  return { isFilter: !prevState.isFilter };
                })
              }
            >
              <i className="la la-filter icon" />
              <input type="text" placeholder="Filter" readOnly />
            </div>
            {isFilter && (
              <div className="comments-filter-item">
                {FilterItems.map((Filter, index) => (
                  <div className="select-item" key={index}>
                    <input
                      className="styled-checkbox"
                      id={`checkbox-${Filter.value}`}
                      type="checkbox"
                      onChange={e => this.handleFilter(e)}
                      value={Filter.value}
                      checked={Filter.checked}
                    />
                    <label htmlFor={`checkbox-${Filter.value}`}>{Filter.name}</label>
                  </div>
                ))}
              </div>
            )}
          </div>
        
      </div>
    );
  };
  render() {
    
    const {
      isFeedModal,
      selectedFeedIndex,
      activeFeedCategory,
      isAddChartActive,
      isCommentsActive,
      activeFeed,
      isShareModalActive,
      dateRangeFilterValue,
      isFilterInChart,
      isContactUs,
      feeds,
      comments,
      tweets,
      headlines,
      isCommentsLoading,
      isTweetsLoading,
      isHeadlinesLoading,
      isInitialLoadOfCommentsForThisTopic,
      isInitialLoadOfTweetsForThisTopic,
      isInitialLoadOfHeadlinesForThisTopic,
      showTweetsTabShortcutButton,
      showHeadlinesTabShortcutButton
    } = this.state;
    const {
      availableCharts,
      sourcesFilter,
      isFeedsLoading,
      topicId,
      updateSelectedCharts,
      isShareReportSending,
      retrieveCategoriesBegin,
      retrieveFeedsBegin,
      feedDetails,
    } = this.props;

    const updatedSourcesFilter = JSON.parse(JSON.stringify(sourcesFilter)).map(source => {
      source.checked = true;
      return source;
    });
    return (
      <div className="container-fluid">
        <div className="d-flex dashboard-container">
          <div className="home-analytics w-100">
            <div className="analytics-menu">
              {topicId.length > 0 && (
                <React.Fragment>
                  <p>Overview</p>
                  {availableCharts.length > 0 && (
                    <div className="menu-item">
                      {isFilterInChart && (
                        <Button
                          icon="refresh"
                          label="Reset"
                          onClick={() => this.resetChartFilter()}
                        />
                      )}
                      <Button
                        icon="envelope"
                        label="Share"
                        onClick={() => this.setState({ isShareModalActive: true })}
                      />
                      <Button icon="download" label="PDF" onClick={() => this.exportToPdf()} />
                      <Filter
                        sourcesFilter={updatedSourcesFilter}
                        filterBySource={e => this.filterBySource(e)}
                      />
                      <Calender setDateRange={e => this.setDateRangeFilter(e)} />
                    </div>
                  )}
                </React.Fragment>
              )}
            </div>
            <div className="analytics-container" ref={this.chartContainer}>
              <div className="row chart-container">
                {topicId && topicId.length > 0 ? (
                  availableCharts.map(
                    charts =>
                      charts.status && (
                        <React.Fragment key={charts.chartId}>
                          <RenderAvailableChart
                            cookies={this.props.cookies}
                            chart={{ ...charts }}
                            selectedTopicId={topicId && topicId.length ? [...topicId] : []}
                            dateFilterValue={dateRangeFilterValue}
                            updateSelectedCharts={e => updateSelectedCharts(e)}
                            source={this.state.sourcesFilter}
                            filterBySentiments={e => this.filterBySentimentsFromChart(e)}
                          />
                        </React.Fragment>
                      ),
                  )
                ) : (
                  <NoTopic
                    title="Almost There"
                    message="Add topics and analyse the sentiments and emotions here."
                    icons={['area-chart', 'pie-chart', 'bar-chart']}
                  >
                    <Link to="/dashboard/topics">
                      <button className="btn-add-topic">ADD TOPIC</button>
                    </Link>
                  </NoTopic>
                )}
                {topicId && topicId.length > 0 && availableCharts.length > 0 && (
                  <div
                    className="chart-content"
                    data-html2canvas-ignore="true"
                    onClick={() => this.setState({ isAddChartActive: true })}
                  >
                    <div className="add-chart-container">
                      <div className="add-char-inner">
                        <span>
                          <i className="la la-plus" /> Add Chart
                        </span>
                      </div>
                    </div>
                  </div>
                )}
                {topicId && topicId.length > 0 && !availableCharts.length && (
                  <div className="no-feed-comment-data">
                    <NoTopic
                      title="Something’s Missing"
                      message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                      icons={['exclamation-circle']}
                    >
                      <button className="btn-no-data" onClick={() => this.showContactUs()}>
                        CONTACT US
                      </button>
                    </NoTopic>
                  </div>
                )}
              </div>
            </div>
            <div
              className={`toggle-button ${isCommentsActive ? 'active' : ''}`}
              onClick={() =>
                {
                  ReactGA.event(
                    {
                      category : this.state.activeFeed ,
                      action : 'User with id ' + this.props.cookies.cookies.userMeta+' is viewing '+this.state.activeFeed
                    }
                  ) 
                  this.setState(prevState => {
                  return { isCommentsActive: !prevState.isCommentsActive };
                
                }
                )}
              }
            >
              <i className={`la la-chevron-circle-${isCommentsActive ? 'right' : 'left'}`} />
            </div>
          </div>
          <div className={`home-feeds flex-shrink-1 ${isCommentsActive ? 'active' : ''}`}>
            <div className={`comments shrink-button ${isCommentsActive ? 'hidden' : 'active'}`}>
              <ul>
                <li onClick={() => this.handleCommentView('comments')} title="Social Media Comments">
                  <i className="la la-commenting" />
                </li>
                <li onClick={() => this.handleCommentView('feeds')} title="Social Media Posts">
                  <i className="la la-rss-square" />
                </li>
                {showTweetsTabShortcutButton && (
                  <li onClick={() => this.handleCommentView('tweets')} title="Tweets">
                    <i className="la la-twitter" />
                  </li>
                )}
                {showHeadlinesTabShortcutButton && (
                  <li onClick={() => this.handleCommentView('headlines')} title="Headlines">
                    <i className="la la-newspaper-o" />
                  </li>
                )}
              </ul>
            </div>
            <div className={`feeds-header ${!isCommentsActive ? 'hidden' : ''}`}>
            {this.actionButtons(activeFeed)}
              <ul>
                {this.state.feedMenu.map((i, index) => (
                  <li
                    className={activeFeed === i ? 'active' : ''}
                    onClick={() =>{
                      ReactGA.event(
                        {
                          category : activeFeed,
                          action : 'User with id ' + this.props.cookies.cookies.userMeta+' is viewing '+activeFeed
                        }
                      ) 
                      this.setState({ activeFeed: i })}}
                    key={index}
                  >
                    {i}
                  </li>
                ))}
              </ul>
            </div>
            <div className={`feeds-container  ${!isCommentsActive ? 'hidden' : ''}`}>
              <ul>
                <li className={` ${activeFeed === 'comments' ? 'active' : 'disabled'}`}>                  
                  <div className={`feed-comments`}>
                    {topicId && topicId.length > 0 ? (
                      <React.Fragment>
                        {isInitialLoadOfCommentsForThisTopic && <Loader />}
                        {!isInitialLoadOfCommentsForThisTopic && comments.data.length > 0 &&
                          comments.data.map((userComment, index) => (
                            <FeedComments
                              cookies={this.props.cookies.cookies}
                              userComment={userComment}
                              key={index}
                              updateRelevance={e => this.setCommentsRelevant(e)}
                            />
                          ))}
                        {!isCommentsLoading && !isInitialLoadOfCommentsForThisTopic && !comments.data.length && (
                          <div className="no-feed-comment-data">
                            <NoTopic
                              title="No Articles available"
                              message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                              icons={['exclamation-circle']}
                            >
                              <button className="btn-no-data" onClick={() => this.showContactUs()}>
                                CONTACT US
                              </button>
                            </NoTopic>
                          </div>
                        )}
                      </React.Fragment>
                    ) : (
                      <NoTopic
                        title="It’s Empty"
                        message="Add topics and the related comments will appear here."
                        icons={['comments']}
                      >
                        <Link to="/dashboard/topics">
                          <button className="btn-add-topic">ADD TOPIC</button>
                        </Link>
                      </NoTopic>
                    )}
                    {!isInitialLoadOfCommentsForThisTopic && comments['count'] && comments['count'] > comments['data'].length && (
                      <button
                        type="button"
                        className={`btn btn-loadmore ${isCommentsLoading ? 'disabled' : ''}`}
                        onClick={() => this.props.loadMoreComments()}
                      >
                        {isCommentsLoading ? (
                          <React.Fragment>
                            <i className="la la-spinner la-spin" /> Loading
                          </React.Fragment>
                        ) : (
                          'LOAD MORE'
                        )}
                      </button>
                    )}
                  </div>
                </li>
                <li className={` ${activeFeed === 'feeds' ? 'active-feeds' : 'disabled-feeds'}`}>
                  <div className={`feed-comments`}>
                    {topicId && topicId.length > 0 ? (
                      <React.Fragment>
                        {retrieveFeedsBegin && <Loader />}

                        {feeds['data'].length > 0 &&
                          feeds['data'].map((feed, index) => (
                            <Feeds
                              feed={feed}
                              key={index}
                              cookies={this.props.cookies.cookies}
                              updateRelevanceFeed={e => this.setFeedsRelevant(e)}
                              handleFeedDetails={e => this.handleFeedDetails(e, index)}
                            />
                          ))}
                        {!retrieveFeedsBegin && !feeds.data.length && (
                          <div className="no-feed-comment-data">
                            <NoTopic
                              title="No Articles available"
                              message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                              icons={['exclamation-circle']}
                            >
                              <button className="btn-no-data" onClick={() => this.showContactUs()}>
                                CONTACT US
                              </button>
                            </NoTopic>
                          </div>
                        )}
                      </React.Fragment>
                    ) : (
                      <NoTopic
                        title="It’s Empty"
                        message="Add topics and the related comments will appear here."
                        icons={['rss-square']}
                      >
                        <Link to="/dashboard/topics">
                          <button className="btn-add-topic">ADD TOPIC</button>
                        </Link>
                      </NoTopic>
                    )}
                    {feeds['count'] && feeds['count'] > feeds['data'].length && (
                      <button
                        type="button"
                        className={`btn btn-loadmore ${isFeedsLoading ? 'disabled' : ''}`}
                        onClick={() =>{
                          ReactGA.event(
                            {
                              category : 'feed' ,
                              action : `more feeds were loaded by ${this.props.cookies.cookies.userMeta} `
                            })
                           this.props.loadMoreFeeds()}}
                      >
                        {isFeedsLoading ? (
                          <React.Fragment>
                            <i className="la la-spinner la-spin" /> Loading
                          </React.Fragment>
                        ) : (
                          'LOAD MORE'
                        )}
                      </button>
                    )}
                  </div>
                </li>

                <li className={` ${activeFeed === 'tweets' ? 'active' : 'disabled'}`}>
                    <TweetsTab 
                      tweets={tweets}
                      loadMoreTweets={() => this.props.loadMoreTweets()}
                      isTweetsLoading={isTweetsLoading}
                      cookies={this.props.cookies}
                      updateTweetsRelevant={this.props.updateTweetsRelevant}
                      retrieveCategoriesBegin={this.props.retrieveCategoriesBegin}
                      isInitialLoadOfTweetsForThisTopic={isInitialLoadOfTweetsForThisTopic}
                      topicId={topicId}
                    />
                </li>

                <li className={` ${activeFeed === 'headlines' ? 'active' : 'disabled'}`}>
                    <HeadlinesTab 
                      headlines={headlines}
                      loadMoreHeadlines={() => this.props.loadMoreHeadlines()}
                      isHeadlinesLoading={isHeadlinesLoading}
                      cookies={this.props.cookies}
                      updateHeadlinesRelevant={this.props.updateHeadlinesRelevant}
                      retrieveCategoriesBegin={this.props.retrieveCategoriesBegin}
                      isInitialLoadOfHeadlinesForThisTopic={isInitialLoadOfHeadlinesForThisTopic}
                      topicId={topicId}
                    />
                </li>

              </ul>
            </div>
          </div>
          {isFeedModal && (
            <FeedDetails
              show={isFeedModal}
              toggleFeedDetails={() => this.setState({ isFeedModal: false })}
              feed={feeds['data'] ? feeds['data'][selectedFeedIndex] : []}
              feedDetails={feedDetails}
              updateRelevanceFeed={e => this.setFeedsRelevant(e)}
              loadMoreFeedDetails={() => this.loadMoreFeedDetails()}
              activeFeedCategory={activeFeedCategory}
              retrieveselectedSentmentFeedDetails={e => this.retrieveselectedSentmentFeedDetails(e)}
              updateRelevance={e => this.setCommentsRelevant(e)}
              cookies={this.props.cookies.cookies}
            />
          )}
        </div>
        {isAddChartActive && (
          <AddChartModal
            cookies={this.props.cookies.cookies}
            isAddChartActive={isAddChartActive}
            availableCharts={availableCharts}
            handleAddChart={e => this.setState({ isAddChartActive: e })}
            updateAvailableChart={e => this.updateAvailableChart(e)}
          />
        )}
        {isShareModalActive && (
          <ShareReportModal
            show={isShareModalActive}
            isReportSending={isShareReportSending}
            shareReportStatus={this.props.shareReportStatus}
            sendReportFile={emailList => this.sendReport(emailList)}
            toggleShareReportModal={e => this.setState({ isShareModalActive: e })}
          />
        )}
        {isContactUs && (
          <ContactUs show={isContactUs} toggleModal={e => this.setState({ isContactUs: e })} />
        )}
      </div>
    );
  }
}

class RenderAvailableChart extends Component {
  removeChart = chartId => {
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.cookies.userMeta} deleted chart with id ${chartId}`
      }
    )
    this.props.updateSelectedCharts([{ chartId, status: false }]);
  };
  render() {
    const { chart, dateFilterValue, selectedTopicId, source, filterBySentiments } = this.props;
    switch (chart.chartType) {
      case 'areaspline':
        return (
          <div className="chart-content trend">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <AreaChart
              title={chart.name}
              chartId={`chart${chart.chartId}`}
              selectedTopicId={[...selectedTopicId]}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
              filterBySentiments={e => filterBySentiments(e)}
            />
          </div>
        );
      case 'line':
        return (
          <div className="chart-content trend">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <LineChart
              title={chart.name}
              chartId={`chart${chart.chartId}`}
              selectedTopicId={[...selectedTopicId]}
              source={[...source]}
              dateRangeFilterValue={dateFilterValue}
              filterBySentiments={e => filterBySentiments(e)}
            />
          </div>
        );
      case 'donut':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <PieChart
              title={chart.name}
              chartId={`chart${chart.chartId}`}
              selectedTopicId={[...selectedTopicId]}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'spider':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <SpiderChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'bar':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <BarChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'map':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <WorldMapChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'column':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <ColumnChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'sourcespercentcolumn':
        return (
          <div className="chart-content trend">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <SourcesPercentColumnChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'wordcloud':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <WordCloud
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'sentiments':
        return (
          <div className="chart-emotions">
            <i
              className="la la-times-circle la-close"
              onClick={() => this.removeChart(chart.chartId)}
            />
            <SentimentsChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );

      case 'emotions':
        return (
          <div className="row-emotions">
            <i
              className="la la-times-circle la-close"
              onClick={() => this.removeChart(chart.chartId)}
            />
            <EmotionsChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      default:
        return <React.Fragment></React.Fragment>;
    }
  }
}

export default Dashboard;
