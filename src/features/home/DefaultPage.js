import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import Dashboard from './components/Dashboard';
import { chartIcons } from './mock';
import { Compare } from './components/Compare';
import moment, { relativeTimeThreshold } from 'moment';
import { toast } from 'react-toastify';
import * as FileSaver from 'file-saver';
import { 
  UNIMPLEMENTED_CHART_IDS,
  CSV_ARTICLE_TYPE_BY_ARTICLE_TYPE 
} from './redux/constants';
import { GaContext } from '../../googleAnalyticsContext/GaContext';

const pageLimit = 10;
export class DefaultPage extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state = {
    activeTopicCompareData: {},
    topicCompareData: [],
    topicDropdown: {},
    allCategories: [],
    availableCharts: [],
    selectedTopicID: [],
    comments: { data: {}, count: null },
    commentsFeedsFilterDate: {
      fromDate: moment()
        .subtract(6, 'days')
        .format('YYYY-MM-DD'),
      toDate: moment().format('YYYY-MM-DD'),
    },
    currentCommentsPage: 1,
    currentFeedsPage: 1,
    currentTweetsPage: 1,
    currentHeadlinesPage: 1,
    isCommentsLoading: false,
    isFeedsLoading: false,
    isTweetsLoading: false,
    isHeadlinesLoading: false,
    isRelevanceUpdating: false,
    isShareReportSending: false,
    isInitialLoadOfCommentsForThisTopic: false,
    isInitialLoadOfTweetsForThisTopic: false,
    isInitialLoadOfHeadlinesForThisTopic: false,
    sentiments: [],
    sources: [],
    feeds: { data: {}, count: null },
    tweets: { data: {}, count: null},
    headlines: {data: {}, count: null},
    isChartUpdating: false,
    csvData: '',
    isDownloadingCsv: false,
    csvArticleType: '',
    selectedTopicName: [],
    shareReportStatus:false
  };

  updateSelectedCharts = selectedCharts => {
    const { updateSelectedChartsList } = this.props.actions;
    this.setState(
      {
        isChartUpdating: true,
      },
      () => {
        updateSelectedChartsList(selectedCharts);
      },
    );
  };

  updateFeedsRelevant = feedId => {
    const { updateFeedsRelevant } = this.props.actions;
    this.setState({ isRelevanceUpdating: true });
    updateFeedsRelevant(feedId);
  };

  getCsvData = articleType => {
    this.setState(
      {
        isDownloadingCsv: true,
        csvArticleType: articleType,
      },
      () => {
        const sources = this._getSourcesListFromLocalStorageOrGetDefaultList();
        this.props.actions.retrieveCsv({
          topicId: this.state.selectedTopicID,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sources: sources.length
            ? sources.join(',').toLocaleLowerCase().trim()
            : 'all sources',
          type: CSV_ARTICLE_TYPE_BY_ARTICLE_TYPE[articleType],
        });
      },
    );
  };

  loadMoreComments = () => {
    this.setState(
      prevState => {
        return {
          currentCommentsPage: prevState.currentCommentsPage + 1,
          isCommentsLoading: true,
        };
      },
      () => {
        this.props.actions.retrieveAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentCommentsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };


  loadMoreTweets = () => {
    this.setState(
      prevState => {
        return {
          currentTweetsPage: prevState.currentTweetsPage + 1,
          isTweetsLoading: true,
        };
      },
      () => {
        this.props.actions.retrieveAllTweets({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentTweetsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments
        });
      },
    );
  };

  loadMoreHeadlines = () => {
    this.setState(
      prevState => {
        return {
          currentHeadlinesPage: prevState.currentHeadlinesPage + 1,
          isHeadlinesLoading: true,
        };
      },
      () => {
        this.props.actions.retrieveAllHeadlines({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentHeadlinesPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources
        }); 
      },
    );
  };

  getCategoryModalData = () => {
    this.props.actions.retrieveAllCategories();
  };

  getCompareData = (props = {}) => {
    this.props.actions.retrieveCompare({
      topicId: props.topicId ? props.topicId : this.state.selectedTopicID,
      topicIndex: props.topicIndex,
      fromDate: props.dateRange.fromDate,
      toDate: props.dateRange.toDate,
      sources: props.selectedSource,
      selectedTopicName: props.selectedTopicName ? props.selectedTopicName : [],
    });
  };

  loadMoreFeeds = () => {
    this.setState(
      prevState => {
        return {
          currentFeedsPage: prevState.currentFeedsPage + 1,
          isFeedsLoading: true,
        };
      },
      () => {
        this.props.actions.retrieveAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentFeedsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };

  commentsFeedsFilterByDate = date => {
    this.setState(
      { 
        commentsFeedsFilterDate: date,
        currentCommentsPage:     1,
        currentFeedsPage:        1,
        currentTweetsPage:       1,
        currentHeadlinesPage:    1,
        isCommentsLoading:       true,
        isTweetsLoading:         true,
        isHeadlinesLoading:      true
      },
      () => {
        this.props.actions.filterAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentCommentsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
        this.props.actions.filterAllHeadlines({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentHeadlinesPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
        this.props.actions.filterAllTweets({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentTweetsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments
        });
        this.props.actions.filterAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentFeedsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };

  resetFilterInChart = () => {
    this.setState(
      prevState => {
        return {
          currentCommentsPage: 1,
          currentFeedsPage: 1,
          currentTweetsPage: 1,
          currentHeadlinesPage: 1,
          isCommentsLoading: true,
          isTweetsLoading: true,
          isHeadlinesLoading: true,
          sentiments: [],
          sources: [],
        };
      },
      () => {
        this.props.actions.filterAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentCommentsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
        this.props.actions.filterAllHeadlines({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentHeadlinesPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
        this.props.actions.filterAllTweets({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentTweetsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments
        });
        this.props.actions.filterAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentFeedsPage,
          pageLimit: pageLimit,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: this.state.sources,
        });
      },
    );
  };

  sendEmailReport = formData => {
    const { createShareReportFile } = this.props.actions;
    this.setState(
      {
        isShareReportSending: true,
      },
      () => {
        createShareReportFile(formData);
      },
    );
  };

  filter = filterObject => {
    const sources = filterObject.sources 
      ? filterObject.sources 
      : this.state.sources;
    this.setState(
      prevState => {
        return {
          currentCommentsPage: 1,
          currentFeedsPage: 1,
          currentTweetsPage: 1,
          currentHeadlinesPage: 1,        
          isCommentsLoading: true,
          isTweetsLoading: true,
          isHeadlinesLoading: true,
          [filterObject.key]: filterObject.value,
        };
      },
      () => {
        this.props.actions.filterAllComments({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentCommentsPage,
          pageLimit: pageLimit,
          fromDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.fromDate,
          toDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: sources
        });
        this.props.actions.filterAllHeadlines({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentHeadlinesPage,
          pageLimit: pageLimit,
          fromDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.fromDate,
          toDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: sources
        });
        this.props.actions.filterAllTweets({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentTweetsPage,
          pageLimit: pageLimit,
          fromDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.fromDate,
          toDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments
        });
        this.props.actions.filterAllFeeds({
          topicId: this.state.selectedTopicID,
          currentPage: this.state.currentFeedsPage,
          pageLimit: pageLimit,
          fromDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.fromDate,
          toDate: filterObject.date
            ? filterObject.date
            : this.state.commentsFeedsFilterDate.toDate,
          sentiments: this.state.sentiments,
          sources: sources
        });
      },
    );
  };

  download = text => {
    const { commentsFeedsFilterDate, csvArticleType } = this.state;
    const filename = `${csvArticleType}_${commentsFeedsFilterDate.fromDate}_${commentsFeedsFilterDate.toDate}.csv`;
    const blob = new Blob([text], { type: 'text/csv' });
    FileSaver.saveAs(blob, filename);
    this.setState({
      csvData: '',
    });
  };

  _getSourcesListFromLocalStorageOrGetDefaultList = () => {
    const savedFilterData = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (savedFilterData && savedFilterData.source) {
      return savedFilterData.source;
    }
    else {
      return [];
    }
  }

  _getSentimentsListFromLocalStorageOrGetDefaultList = () => {
    const savedFilterData = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (savedFilterData && savedFilterData.FilterItems && savedFilterData.FilterItems.length) {
      return this._convertFilterItemsToArrayOfStrings(savedFilterData.FilterItems);
    }
    else {
      return ['all'];
    }
  }

  _convertFilterItemsToArrayOfStrings(FilterItems) {
    let sentiments = [];
    for (let i=0; i<FilterItems.length; i++) {
      let FilterItem = FilterItems[i];
      if (FilterItem.name === "all" && FilterItem.checked) {
        return ['all'];
      }
      else if (FilterItem.checked) {
        sentiments.push(FilterItem.value);
      }
    }
    return sentiments;
  }
  static contextType =GaContext
 
  componentDidMount() {

    const {ReactGA} = this.context
    ReactGA.pageview('/dashboard')
    const { retrieveAllCharts } = this.props.actions;
    const savedFilterData = JSON.parse(localStorage.getItem('dashboardFilter'));
    const { commentsFeedsFilterDate } = this.state;
    if (savedFilterData && Object.keys(savedFilterData).length) {
      if (savedFilterData.dateRangeFilterValue) {
        commentsFeedsFilterDate['toDate'] = savedFilterData.dateRangeFilterValue.toDate;
        commentsFeedsFilterDate['fromDate'] = savedFilterData.dateRangeFilterValue.fromDate;
      }
      this.setState(
        {
          sources: this._getSourcesListFromLocalStorageOrGetDefaultList(),
          sentiments: this._getSentimentsListFromLocalStorageOrGetDefaultList(),
          commentsFeedsFilterDate,
        },
        () => {
          retrieveAllCharts();
        },
      );
    } else {
      retrieveAllCharts();
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      JSON.stringify(this.state.availableCharts) !==
      JSON.stringify(newProps.home.availableCharts)
    ) {
      const updatedChartData = JSON.parse(JSON.stringify(newProps.home.availableCharts)).map(
        chart => {
          chart['icon'] = chartIcons[chart.chartType.toLowerCase()];
          return chart;
        },
      );

      const filteredAndUpdatedChartData = this._filterOutUnimplementedCharts(updatedChartData);

      this.setState({
        availableCharts: [...filteredAndUpdatedChartData],
      });
    }

    if (newProps.home.updatedChartId.length && this.state.isChartUpdating) {
      const updatedChartList = newProps.home.updatedChartId.map(chart => chart.chartId);
      const trueCount = newProps.home.updatedChartId.filter(chart => chart.status === true);
      if (trueCount.length === updatedChartList.length) {
        toast.success('Chart(s) has been successfully added to the dashboard!');
      } else if (!trueCount.length) {
        toast.success('Chart(s) has been successfully removed from the dashboard!');
      } else {
        toast.success(
          'Chart(s) has been successfully added to and removed from the dashboard!',
        );
      }

      this.setState({
        isChartUpdating: false,
      });
    }

    if (this._receivedPropsSignalingReadyToUpdateDashboardForNewTopic(newProps)) {
      const resetCurrentPage = 1;
      const selectedTopicID = newProps.home.activeTopic;
      this.setState({
        topicDropdown: newProps.home.headerTopicData,
        selectedTopicID,
        isInitialLoadOfCommentsForThisTopic: true,
        isCommentsLoading: true,
        isInitialLoadOfTweetsForThisTopic: true,
        isTweetsLoading: true,
        isInitialLoadOfHeadlinesForThisTopic: true,
        isHeadlinesLoading: true,
        currentCommentsPage: resetCurrentPage,
        currentFeedsPage: resetCurrentPage,
        currentTweetsPage: resetCurrentPage,
        currentHeadlinesPage: resetCurrentPage,
      }, () => {
        const postData = {
          topicId: selectedTopicID,
          currentPage: resetCurrentPage,
          fromDate: this.state.commentsFeedsFilterDate.fromDate,
          toDate: this.state.commentsFeedsFilterDate.toDate,
          sources: this.state.sources,
          sentiments: this.state.sentiments,
          pageLimit,
        };
        this.props.actions.retrieveAllComments(postData);
        this.props.actions.retrieveAllHeadlines(postData);
        this.props.actions.retrieveAllFeeds(postData);
        this.props.actions.retrieveAllTweets(postData);
      });
    }

    if (
      newProps.home.topicDropdown.length &&
      JSON.stringify(newProps.home.headerTopicData) !==
        JSON.stringify(this.state.topicDropdown)
    ) {
      this.setState({
        topicDropdown: newProps.home.headerTopicData,
      });
    }

    if (
      Object.keys(newProps.home.allFeeds).length &&
      JSON.stringify(this.state.feeds) !== JSON.stringify(newProps.home.allFeeds)
    ) {
      this.setState({
        isFeedsLoading: false,
        feeds: newProps.home.allFeeds,
      });
    }
    
    this._updateCommentRelatedStateIfNeeded(newProps);
    this._updateTweetRelatedStateIfNeeded(newProps);
    this._updateHeadlineRelatedStateIfNeeded(newProps);
    this._updateStateIfRelevanceUpdated(newProps);

    if (newProps.home.sendShareReportSuccess && this.state.isShareReportSending) {
      toast.success('Report shared successfully.');
      this.setState({
        isShareReportSending: false,
      });
    }
    if (newProps.home.sendShareReportError && this.state.isShareReportSending) {
      toast.error('Failed to share report.');
      this.setState({
        isShareReportSending: false,
      });
    }
    if (
      JSON.stringify(newProps.home.activeTopicCompareData) !==
      JSON.stringify(this.state.activeTopicCompareData)
    ) {
      this.setState({
        activeTopicCompareData: newProps.home.activeTopicCompareData,
      });
    }
    if (
      newProps.home.selectedTopicName.length &&
      JSON.stringify(newProps.home.selectedTopicName) !==
        JSON.stringify(this.state.selectedTopicName)
    ) {
      this.setState({
        selectedTopicName: newProps.home.selectedTopicName,
      });
    }
    if (
      newProps.home.topicCompareData.length &&
      JSON.stringify(newProps.home.topicCompareData) !==
        JSON.stringify(this.state.topicCompareData)
    ) {
      this.setState({
        topicCompareData: newProps.home.topicCompareData,
      });

      if (
        (newProps.sendShareReportSuccess || newProps.sendShareReportError) &&
        this.state.isShareReportSending
      ) {
        this.setState({
          isShareReportSending: false,
        });
      }
    }

    if (this.hasPropsForCSVDownload(newProps)) {
      this.componentWillReceivePropsForCSVDownload(newProps);
    }
  }

  _receivedPropsSignalingReadyToUpdateDashboardForNewTopic(newProps) {
    if (newProps.home.activeTopic.length &&
    JSON.stringify(newProps.home.activeTopic) !==
      JSON.stringify(this.state.selectedTopicID) && 
      (! newProps.home.retrieveActiveTopicBegin && newProps.home.retrieveActiveTopicSucess) && 
      JSON.stringify(newProps.home.retrieveActiveTopicLastSuccessfulTopic) === JSON.stringify(newProps.home.activeTopic)) {
        return true;
      }
      else {
        false;
      }
  }

  _updateCommentRelatedStateIfNeeded(newProps) {
    let updatedState = {};
    if (this._hasNewListOfCommentsReceivedFromProps(newProps)) {
      updatedState['isCommentsLoading']                   = false;
      updatedState['comments']                            = newProps.home.allComments;
      updatedState['isInitialLoadOfCommentsForThisTopic'] = false;
    }
    else if (this._didCommentsRequestComplete(newProps)) {
      updatedState['isCommentsLoading']                   = false;
      updatedState['isInitialLoadOfCommentsForThisTopic'] = false;
    }
    
    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }
  
  _hasNewListOfCommentsReceivedFromProps(newProps) {
    if (Object.keys(newProps.home.allComments).length &&
        JSON.stringify(this.state.comments) !== JSON.stringify(newProps.home.allComments)
      ) {
        return true;
      }
      else {
        return false;
      }
  }

  _didCommentsRequestComplete(newProps) {
    if ((newProps.home.retrieveCommentsSuccess 
      || newProps.home.retrieveCommentsFailure) && this.state.isCommentsLoading) {
        return true;
    }
    else {
      return false;
    }
  }

  _updateTweetRelatedStateIfNeeded(newProps) {
    let updatedState = {};
    if (this._hasNewListOfTweetsReceivedFromProps(newProps)) {
      updatedState['isTweetsLoading']                   = false;
      updatedState['tweets']                            = newProps.home.allTweets;
      updatedState['isInitialLoadOfTweetsForThisTopic'] = false;
    }
    else if (this._didTweetsRequestComplete(newProps)) {
      updatedState['isTweetsLoading']                   = false;
      updatedState['isInitialLoadOfTweetsForThisTopic'] = false;
    }
    
    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  _hasNewListOfTweetsReceivedFromProps(newProps) {
    if (Object.keys(newProps.home.allTweets).length &&
        JSON.stringify(this.state.tweets) !== JSON.stringify(newProps.home.allTweets)
      ) {
        return true;
      }
      else {
        return false;
      }
  }

  _didTweetsRequestComplete(newProps) {
    if ((newProps.home.retrieveTweetsSuccess 
      || newProps.home.retrieveTweetsFailure) && this.state.isTweetsLoading) {
        return true;
    }
    else {
      return false;
    }
  }

  _updateHeadlineRelatedStateIfNeeded(newProps) {
    let updatedState = {};
    if (this._hasNewListOfHeadlinesReceivedFromProps(newProps)) {
      updatedState['isHeadlinesLoading']                   = false;
      updatedState['headlines']                            = newProps.home.allHeadlines;
      updatedState['isInitialLoadOfHeadlinesForThisTopic'] = false;
    }
    else if (this._didHeadlinesRequestComplete(newProps)) {
      updatedState['isHeadlinesLoading']                   = false;
      updatedState['isInitialLoadOfHeadlinesForThisTopic'] = false;
    }

    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  _didHeadlinesRequestComplete(newProps) {
    if ((newProps.home.retrieveHeadlinesSuccess 
      || newProps.home.retrieveHeadlinesFailure) && this.state.isHeadlinesLoading) {
        return true;
    }
    else {
      return false;
    }
  }

  _hasNewListOfHeadlinesReceivedFromProps(newProps) {
    if (Object.keys(newProps.home.allHeadlines).length &&
        JSON.stringify(this.state.headlines) !== JSON.stringify(newProps.home.allHeadlines)
      ) {
        return true;
      }
      else {
        return false;
      }
  }

  _updateStateIfRelevanceUpdated(newProps) {
    if (this.state.isRelevanceUpdating) {
      if (newProps.home.updateCommentsRelevantSuccess || newProps.home.updateHeadlinesRelevantSuccess || newProps.home.updateTweetsRelevantSuccess || newProps.home.updateFeedsRelevantSuccess) {
        toast.success('Relevance has been updated');
        this.setState({
          isRelevanceUpdating: false,
        });
      }
      else if (newProps.home.updateCommentsRelevantFailure || newProps.home.updateHeadlinesRelevantFailure || newProps.home.updateFeedsRelevantFailure || newProps.home.updateTweetsRelevantFailure) {
        toast.error('Relevance has not been updated');
        this.setState({
          isRelevanceUpdating: false,
        });
      } 
    }
  }

  hasPropsForCSVDownload(newProps) {
    if (
      newProps.home.csvData !== this.state.csvData &&
      newProps.home.retrieveCsvSuccess &&
      this.state.isDownloadingCsv
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  componentWillReceivePropsForCSVDownload(newProps) {
    this.setState(
      {
        csvData: newProps.home.csvData,
        isDownloadingCsv: false,
      },
      () => {
        if (this.state.csvData) {
          this.download(this.state.csvData);
        }
      },
    );
  }

  _filterOutUnimplementedCharts = (chartData) => {
    return chartData.filter(chart => ! UNIMPLEMENTED_CHART_IDS.includes(chart.chartId));
  }

  render() {
  
    const {
      comments,
      isCommentsLoading,
      isTweetsLoading,
      isHeadlinesLoading,
      feeds,
      tweets,
      headlines,
      isFeedsLoading,
      selectedTopicID,
      activeTopicCompareData,
      topicCompareData,
      isShareReportSending,
      topicDropdown,
      availableCharts,
      selectedTopicName,
      isInitialLoadOfCommentsForThisTopic,
      isInitialLoadOfTweetsForThisTopic,
      isInitialLoadOfHeadlinesForThisTopic
    } = this.state;
    const {
      updateCommentsRelevant,
      updateTweetsRelevant,
      updateHeadlinesRelevant,
      retrieveAllCategories,
      retriveShareReportStatus,
      retrieveFeedDetails,
    } = this.props.actions;
    const availableSources = this.props.home.allSources.filter(source => source.selected);
    const { activeTab, shareReportStatus } = this.props.home;
    return (
      <div className="home-default-page">
        {activeTab && activeTab === 'dashboard' ? (
          <Dashboard
            cookies={this.props.cookies}
            comments={comments}
            headlines={headlines}
            feeds={feeds}
            tweets={tweets}
            isCommentsLoading={isCommentsLoading}
            isTweetsLoading={isTweetsLoading}
            isHeadlinesLoading={isHeadlinesLoading}
            isInitialLoadOfCommentsForThisTopic={isInitialLoadOfCommentsForThisTopic}
            isInitialLoadOfTweetsForThisTopic={isInitialLoadOfTweetsForThisTopic}
            isInitialLoadOfHeadlinesForThisTopic={isInitialLoadOfHeadlinesForThisTopic}
            loadMoreComments={() => this.loadMoreComments()}
            loadMoreTweets={() => this.loadMoreTweets()}
            loadMoreHeadlines={() => this.loadMoreHeadlines()}
            filter={filterObj => this.filter(filterObj)}
            getCsvData={e => this.getCsvData(e)}
            sourcesFilter={availableSources}
            availableCharts={availableCharts}
            loadMoreFeeds={() => this.loadMoreFeeds()}
            filterFeedsCommentsByDate={value => this.commentsFeedsFilterByDate(value)}
            isFeedsLoading={isFeedsLoading}
            topicId={selectedTopicID.length ? [...selectedTopicID] : {}}
            updateCommentsRelevant={e =>
              this.setState({ isRelevanceUpdating: true }, () => {
                updateCommentsRelevant(e);
              })
            }
            updateTweetsRelevant={e =>
              this.setState({ isRelevanceUpdating: true }, () => {
                updateTweetsRelevant(e);
              })
            }
            updateHeadlinesRelevant={e =>
              this.setState({ isRelevanceUpdating: true }, () => {
                updateHeadlinesRelevant(e);
              })
            }
            headerTopicData={this.props.home.headerTopicData}
            resetFilterInChart={e => this.resetFilterInChart()}
            updateFeedsRelevant={e => this.updateFeedsRelevant(e)}
            {...this.state}
            updateSelectedCharts={e => this.updateSelectedCharts(e)}
            retrieveShareReportMessage={() =>
              retriveShareReportStatus({ topicId: selectedTopicID })
            }
            sendEmailReport={e => this.sendEmailReport(e)}
            isShareReportSending={isShareReportSending}
            shareReportStatus={shareReportStatus}
            retrieveFeedDetails={e => retrieveFeedDetails(e)}
            feedDetails={this.props.home.feedDetails}
            retrieveCategoriesBegin={this.props.home.retrieveCategoriesBegin}
            retrieveFeedsBegin={this.props.home.retrieveFeedsBegin}
          />
        ) : (
          <Compare
            cookies={this.props.cookies}
            sourcesFilter={availableSources}
            activeTopicCompareData={activeTopicCompareData}
            topicDropdown={topicDropdown}
            topicId={[...selectedTopicID]}
            topicCompareData={topicCompareData}
            categories={this.props.home.allCategories}
            filter={(value, key) => this.filter(value, key)}
            getCompareData={e => this.getCompareData(e)}
            selectedTopicNames={selectedTopicName}
            getCategories={e => retrieveAllCategories(e)}
            getCategoryModalData={e => this.getCategoryModalData(e)}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultPage);
