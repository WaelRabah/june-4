import {
  UPDATE_FEEDS_RELEVANT_BEGIN,
  UPDATE_FEEDS_RELEVANT_SUCCESS,
  UPDATE_FEEDS_RELEVANT_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateFeedsRelevant(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_FEEDS_RELEVANT_BEGIN,
    });
    const url = `${apiEndPoints.feeds.UPDATE_FEED_RELEVANT}?feedId=${props.feedId}&relevance=${props.relevance}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_FEEDS_RELEVANT_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_FEEDS_RELEVANT_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_FEEDS_RELEVANT_BEGIN:
      return {
        ...state,
        updateFeedsRelevantBegin: true,
        updateFeedsRelevantSuccess: false,
        updateCommentsRelevantSuccess: false,
        updateFeedsRelevantFailure: false,
      };
    case UPDATE_FEEDS_RELEVANT_SUCCESS:
      const updatedFeeds = state.allFeeds.data.map(feed => {
        if (feed.feedId === action.data.feedId) {
          feed.relevance = action.data.relevance;
        }
        return feed;
      });
      state.allFeeds.data = updatedFeeds;
      return {
        ...state,
        updateFeedsRelevantBegin: false,
        updateFeedsRelevantSuccess: true,
      };
    case UPDATE_FEEDS_RELEVANT_FAILURE:
      return {
        ...state,
        updateFeedsRelevantBegin: false,
        updateFeedsRelevantFailure: true,
      };
    default:
      return state;
  }
}
