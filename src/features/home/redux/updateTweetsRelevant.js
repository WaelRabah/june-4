import {
    UPDATE_TWEETS_RELEVANT_BEGIN,
    UPDATE_TWEETS_RELEVANT_SUCCESS,
    UPDATE_TWEETS_RELEVANT_FAILURE,
  } from './constants';
  import http from '../../../common/http';
  import { apiEndPoints } from '../../../common/globalConstants';
  
  export function updateTweetsRelevant(props = {}) {
    return dispatch => {
      dispatch({
        type: UPDATE_TWEETS_RELEVANT_BEGIN,
      });
      const url = `${apiEndPoints.tweets.UPDATE_TWEET_RELEVANT}?tweetId=${props.tweetId}&relevance=${props.relevance}`;
      const promise = new Promise((resolve, reject) => {
        const doRequest = http.patch(url);
        doRequest.then(
          res => {
            dispatch({
              type: UPDATE_TWEETS_RELEVANT_SUCCESS,
              data: props,
            });
            resolve(res);
          },
          err => {
            dispatch({
              type: UPDATE_TWEETS_RELEVANT_FAILURE,
            });
            reject(err);
          },
        );
      });
      return promise;
    };
  }
  
  export function reducer(state, action) {
    switch (action.type) {
      case UPDATE_TWEETS_RELEVANT_BEGIN:
        return {
          ...state,
          updateTweetsRelevantBegin: true,
          updateTweetsRelevantSuccess: false,
          updateFeedsRelevantSuccess: false,
          updateTweetsRelevantFailure: false,
        };
      case UPDATE_TWEETS_RELEVANT_SUCCESS:
        const updatedTweets = state.allTweets.data.map(tweet => {
          if (tweet.tweetId === action.data.tweetId) {
            tweet.relevance = action.data.relevance;
          }
          return tweet;
        });
        state.allTweets.data = updatedTweets;
        return {
          ...state,
          updateTweetsRelevantBegin: false,
          updateTweetsRelevantSuccess: true,
        };
      case UPDATE_TWEETS_RELEVANT_FAILURE:
        return {
          ...state,
          updateTweetsRelevantBegin: false,
          updateTweetsRelevantFailure: true,
        };
      default:
        return state;
    }
  }
  