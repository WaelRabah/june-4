import initialState from './initialState';
import { reducer as toggleTab } from './toggleTabs';
import { reducer as chartService } from './chartService';
import { reducer as retrieveAllCategories } from './retrieveAllCategories';
import { reducer as updateSelectedTopic } from './updateSelectedTopics';
import { reducer as retrieveTopicsDropdown } from './retrieveTopicsDropdown';
import { reducer as retrieveAllComments } from './retrieveComments';
import { reducer as retrieveAllTweets } from './retrieveTweets';
import { reducer as retrieveAllHeadlines } from './retrieveHeadlines';
import { reducer as retrieveSourcesFilter } from './retrieveSourcesFilter';
import { reducer as retrieveFeeds } from './retrieveFeeds';
import { reducer as updateTopicsDropdown } from './updateTopicDropDown';
import { reducer as updateCommentsRelevant } from './updateCommentsRelevant';
import { reducer as updateTweetsRelevant } from './updateTweetsRelevant';
import { reducer as updateHeadlinesRelevant } from './updateHeadlinesRelevant';
import { reducer as retrieveCompare } from './retrieveCompare';

import { reducer as updateFeedsRelevant } from './updateFeedsRelevant';
import { reducer as shareReportMessageReducer } from './shareReportMessage';
import { reducer as updateSourcesFilter } from './updateSourcesFilter';
import { reducer as createShareReportFile } from './sendReportFile';
import { reducer as retrieveCsv } from './retrieveCsv';
import { reducer as retrieveFeedDetails } from './retrieveFeedDetails';
const reducers = [
  toggleTab,
  chartService,
  retrieveAllCategories,
  updateSelectedTopic,
  retrieveAllComments,
  retrieveSourcesFilter,
  retrieveFeeds,
  retrieveAllTweets,
  retrieveAllHeadlines,
  retrieveTopicsDropdown,
  updateTopicsDropdown,
  updateCommentsRelevant,
  updateTweetsRelevant,
  updateHeadlinesRelevant,
  retrieveCompare,
  updateFeedsRelevant,
  shareReportMessageReducer,
  updateSourcesFilter,
  createShareReportFile,
  retrieveCsv,
  retrieveFeedDetails,
];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
