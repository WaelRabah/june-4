import {
  UPDATE_HEADLINES_RELEVANT_BEGIN,
  UPDATE_HEADLINES_RELEVANT_SUCCESS,
  UPDATE_HEADLINES_RELEVANT_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateHeadlinesRelevant(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_HEADLINES_RELEVANT_BEGIN,
    });
    const url = `${apiEndPoints.headlines.UPDATE_HEADLINE_RELEVANT}?headlineId=${props.headlineId}&relevance=${props.relevance}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_HEADLINES_RELEVANT_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_HEADLINES_RELEVANT_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_HEADLINES_RELEVANT_BEGIN:
      return {
        ...state,
        updateHeadlinesRelevantBegin: true,
        updateHeadlinesRelevantSuccess: false,
        updateFeedsRelevantSuccess: false,
        updateHeadlinesRelevantFailure: false,
      };
    case UPDATE_HEADLINES_RELEVANT_SUCCESS:
      const updatedHeadlines = state.allHeadlines.data.map(headline => {
        if (headline.headlineId === action.data.headlineId) {
          headline.relevance = action.data.relevance;
        }
        return headline;
      });
      state.allHeadlines.data = updatedHeadlines;
      return {
        ...state,
        updateHeadlinesRelevantBegin: false,
        updateHeadlinesRelevantSuccess: true,
      };
    case UPDATE_HEADLINES_RELEVANT_FAILURE:
      return {
        ...state,
        updateHeadlinesRelevantBegin: false,
        updateHeadlinesRelevantFailure: true,
      };
    default:
      return state;
  }
}
