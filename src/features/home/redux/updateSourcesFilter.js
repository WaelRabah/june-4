import { UPDATE_SOURCE_FILTER } from './constants';
export function updateAvailableSources(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_SOURCE_FILTER,
      data: props,
    });
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_SOURCE_FILTER:
      return {
        ...state,
        allSources: action.data,
      };
    default:
      return state;
  }
}
