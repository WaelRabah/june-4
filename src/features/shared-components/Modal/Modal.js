import React, { useEffect } from 'react';

const Modal = props => {
  function useOutsideClick() {
    function handleClick(event) {
      if (event.target.classList.contains('modal')) {
        props.handleClick();
      }
    }
    function handleEscButton(event) {
      if (event.keyCode === 27) props.handleClick();
    }
    useEffect(() => {
      document.addEventListener('mousedown', handleClick);
      document.addEventListener('keydown', handleEscButton);
      return () => {
        document.removeEventListener('keydown', handleEscButton);
        document.removeEventListener('mousedown', handleClick);
      };
    });
  }
  useOutsideClick();
  return <div className="modal-global-container">{props.children}</div>;
};

export default Modal;
