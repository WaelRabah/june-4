import React from 'react';
import { withFirebase } from '../../../Firebase';
const signOut = firebase => {
  firebase.signOut().then(() => {
    localStorage.removeItem('dashboardFilter');
    window.location.href = '/login';
  });
};
const SignOutButton = ({ firebase }) => (
  <li onClick={() => signOut(firebase)}>
    <span>Log Out</span>
  </li>
);

export default withFirebase(SignOutButton);
