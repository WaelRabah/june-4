import React, { useState, useRef, useEffect } from 'react';
import SelectedCategory from './SelectedCategory';
import SignOutButton from './signOut';
const navMenues = {
  dashboard: ['dashboard', 'compare'],
  topics: ['topics'],
  sources: ['sources'],
  reports: ['reports'],
};

const Header = ({
  props,
  setCategorySelectionModalActive,
  setActiveTopic,
  setActiveTab,
  setIsChangePassword,
}) => {
  const [showSettings, toggleSettings] = useState(false);
  const [toggle, setToggle] = useState('dashboard');
  const [activeRoute, setRoute] = useState('dashboard');
  const handleDashboardToggle = data => {
    setToggle(data);
    setActiveTab(data);
  };

  useEffect(() => {
    let _path = props.location.pathname.split('/');
    _path = _path.length > 1 ? _path[_path.length - 1] : 'dashboard';
    if (navMenues[_path]) {
      setRoute(_path);
      setToggle(
        navMenues[_path].includes(props.App.activeTab) ? props.App.activeTab : navMenues[_path][0],
      );
    }
  }, [props.location]);

  const wrapperRef = useRef(null);
  function useOutsideClick(ref) {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        toggleSettings(false);
      }
    }
    useEffect(() => {
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    });
  }
  useOutsideClick(wrapperRef);
  return (
    <header>
      <div className="container-fluid">
        <div className="row content">
          <SelectedCategory
            isCategorySelectionModalActive={e => setCategorySelectionModalActive(e)}
            topics={props.App.headerTopicData}
            topicsDropDown={props.App.topicDropdown}
            setSelectedTopicActive={e => setActiveTopic(e)}
          />
          <div className="dash-div">
            {navMenues[activeRoute].map((navmenu, index) => (
              <div
                className={['dashboard', toggle === navmenu ? 'active' : ''].join(' ')}
                onClick={() => handleDashboardToggle(navmenu)}
                key={index}
              >
                {navmenu}
              </div>
            ))}
          </div>
          <div
            className={`user ${showSettings ? 'active' : ''}`}
            onClick={() => toggleSettings(!showSettings)}
            ref={wrapperRef}
          >
            <i className="la la-user "></i>
            <div className={`user-block ${showSettings ? 'show' : ''}`}>
              <ul>
                <li onClick={() => setIsChangePassword(true)}>Change Password</li>
                <li>Price Plans</li>
                <SignOutButton />
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
