import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsReact from 'highcharts-react-official';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import moment from 'moment';
HighchartsMore(Highcharts);

class EmotionsChart extends AbstractChart {
  constructor(props) {
    super(props);
    this.state = {
      topicId: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      source: [],
      mapDataFromProps: [],
      chartData: [],
    };
  }

  numberConverter = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  reRenderChartOnChange = props => {
    let { mapOptions } = this.state;
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        mapOptions,
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          topicId: this.state.topicId,
          dateFilterValue: this.state.filterDateRange,
          source: props.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { chartId, selectedTopicId } = this.props;
    if (
      JSON.stringify(nextProps.EmotionsChart[chartId]) !== JSON.stringify(this.state.chartData) &&
      nextProps.selectedTopicId != null
    ) {
      let updatedEmotionsChart = [];
      nextProps.EmotionsChart[chartId].forEach(chart => {
        switch (chart.category) {
          case 'joy':
            updatedEmotionsChart[0] = chart;
            break;
          case 'anger':
            updatedEmotionsChart[1] = chart;
            break;
          case 'fear':
            updatedEmotionsChart[2] = chart;
            break;
          case 'sadness':
            updatedEmotionsChart[3] = chart;
            break;
          default:
            break;
        }
      });
      this.setState(
        {
          mapDataFromProps: [],
          chartData: nextProps.EmotionsChart[chartId],
          topicId: nextProps.selectedTopicId,
          source: nextProps.source,
        },
        () => {
          this.setState({
            mapDataFromProps: updatedEmotionsChart,
          });
        },
      );
    }
    if (Object.keys(nextProps.EmotionsChart['chart103']).length && this.state.chartData.length) {
      let emotionsChart = JSON.parse(JSON.stringify(nextProps.EmotionsChart['chart103']));
      let parsedEmotionChart = {};
      const { mapDataFromProps } = this.state;
      emotionsChart.forEach(item => {
        parsedEmotionChart[item.category] = item.data;
      });
      const updatedChartData = mapDataFromProps.map(chart => {
        chart['chartData'] = { data: parsedEmotionChart[chart.category].map(nodes => nodes[1]) };
        return chart;
      });
      this.setState({ mapDataFromProps: updatedChartData });
    }

    if (this._shouldReRenderChartBasedOnNewProps(nextProps)) {
      this.reRenderChartOnChange(nextProps);
    }
  }

  render() {
    const { mapDataFromProps } = this.state;
    return (
      <React.Fragment>
        {mapDataFromProps.length > 0 &&
          mapDataFromProps.map((chart, index) => (
            <div className="col-3 mentions" key={index}>
              <div className="mention-container">
                <div className="mention-label">
                  <h6>{chart.categoryName}</h6>
                  <span className="like-count">{this.numberConverter(Number(chart.count))}</span>
                  <span className={` data-growth ${chart.change > 0 ? 'up' : 'down'}`}>
                    <i className={`la la-arrow-${chart.change > 0 ? 'up' : 'down'}`} />
                    <label>{chart.change > 0 ? chart.change : Math.abs(chart.change)}%</label>
                  </span>
                </div>
                <div className="chart-block">
                  {chart.chartData && chart.chartData.data.length > 0 && (
                    <Minichart chartData={chart.chartData} />
                  )}
                </div>
              </div>
            </div>
          ))}
      </React.Fragment>
    );
  }
}

class Minichart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapOptions: {
        chart: {
          type: 'column',
          backgroundColor: 'transparent',
          width: 90,
          height: 50,
        },
        series: [],
        legend: {
          enabled: false,
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: {
          enabled: false,
        },
        exporting: {
          enabled: false,
        },
        plotOptions: {
          series: {
            color: '#BCE0FD',
            pointWidth: 5,
            borderWidth: 1,
            borderColor: 'white',
          },
        },
        credits: {
          enabled: false,
        },
        xAxis: {
          visible: false,
        },
        yAxis: {
          visible: false,
        },
      },
    };
  }
  UNSAFE_componentWillReceiveProps(newProps) {
    let { mapOptions } = this.state;
    newProps.chartData && Object.keys(newProps.chartData).length
      ? (mapOptions['series'] = [newProps.chartData])
      : null;
    this.setState({ mapOptions });
  }

  render() {
    const { mapOptions } = this.state;
    return (
      <React.Fragment>
        {mapOptions.series && mapOptions.series.length > 0 && (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    EmotionsChart: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmotionsChart);
