import {
  RETRIEVE_SELECTED_CHART_DETAILS_BEGIN,
  RETRIEVE_SELECTED_CHART_DETAILS_SUCCESS,
  RETRIEVE_SELECTED_CHART_DETAILS_FAILURE,
} from './constants';
import http from '../../../../common/http';
import { apiEndPoints } from '../../../../common/globalConstants';
import moment from 'moment';

function getQueryUnit(date) {
  const startDate = moment(date.fromDate);
  const endDate = moment(date.toDate);
  const diffDays = endDate.diff(startDate, 'days');
  if (diffDays !== 0 && diffDays <= 90) {
    return 'day';
  } else if (diffDays === 0) {
    return '';
  } else {
    return 'month';
  }
}
export function retrieveSelectedChartData(props = {}) {
  const chartData = typeof props === 'object' ? props : { chartId: props };
  const queryUnit = typeof props === 'object' ? getQueryUnit(props.dateFilterValue) : 'day';
  return dispatch => {
    dispatch({
      type: RETRIEVE_SELECTED_CHART_DETAILS_BEGIN,
    });
    const url = `${apiEndPoints.charts.RETRIEVE_CHART_BY_ID}${chartData.chartId}&fromDate=${
      chartData.dateFilterValue ? chartData.dateFilterValue.fromDate : ''
    }&toDate=${chartData.dateFilterValue ? chartData.dateFilterValue.toDate : ''}&unit=${
      ['102', '103'].indexOf(chartData.chartId) !== -1 ? queryUnit : 'total'
    }&topicId=${chartData.topicId}&sources=${
      chartData.source.length ? chartData.source.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_SELECTED_CHART_DETAILS_SUCCESS,
            data: {
              data: res.data.status === 200 ? res.data.data : [],
              chartId: chartData.chartId,
            },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_SELECTED_CHART_DETAILS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_SELECTED_CHART_DETAILS_BEGIN:
      return {
        ...state,
        retrieveChartDataBegin: true,
        retrieveChartDataSuccess: false,
        retrieveChartDataFailure: false,
      };
    case RETRIEVE_SELECTED_CHART_DETAILS_SUCCESS:
      return {
        ...state,
        [`chart${action.data.chartId}`]: action.data.data,
        retrieveChartDataBegin: false,
        retrieveChartDataSuccess: true,
      };
    case RETRIEVE_SELECTED_CHART_DETAILS_FAILURE:
      return {
        ...state,
        retrieveChartDataBegin: false,
        retrieveChartDataFailure: true,
      };

    default:
      return state;
  }
}
