import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { chartColorCode } from './colorCodes';
import moment from 'moment';
class PieChart extends AbstractChart {
  constructor(props) {
    super(props);
    this.state = {
      chartData: [],
      topicId: [],
      mapDataFromProps: [],
      source: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          type: 'pie',
          backgroundColor: 'white',
          marginBottom: 0,
          marginLeft: -20,
          marginRight: 130,
          marginTop: 25,
          width: 370,
          height: 240,
        },

        plotOptions: {
          pie: {
            innerSize: '50%',
            width: 160,
            height: 160,
            size: 180,
            slicedOffset: 0,
            showInLegend: true,
            dataLabels: {
              enabled: true,
              format: '<br>{point.percentage:.1f} %',
              distance: -21,

              style: {
                textOutline: false,
                fontSize: '9px',
                fontWeight: 'normal',
                color: '#fff',
              },
              filter: {
                property: 'percentage',
                operator: '>',
                value: 4,
              },
            },
          },
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          itemMarginBottom: 14,
          y: 20,
          maxHeight: 165,
          width: 100,
          itemStyle: {
            fontSize: '10px',
            lineHeight: '25px',
            fontWeight: 'reguler',
            color: '#999999',
          },
        },
        caption: {
          text: '',
          align: 'center',
          x: -75,
          y: 25,
          verticalAlign: 'middle',
          style: {
            color: '#999999',
            fontSize: '9px',
          },
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: { enabled: false },
        credits: {
          enabled: false,
        },
        xAxis: {
          labels: {
            style: {
              color: '#999999',
              fontSize: '8px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
          },
        },
        yAxis: {
          title: '',
        },
      },
    };
  }

  parseDonutChart = chart => {
    const updatedChart = chart.map(item => {
      let name = item[0].replace('Mentions', '').trim();
      return { name, color: chartColorCode[name.toLowerCase()], y: item[1], selected: true };
    });
    return [{ data: updatedChart }];
  };

  reRenderChartOnChange = props => {
    let { mapOptions } = this.state;
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        mapOptions,
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, title, selectedTopicId } = this.props;
    if (
      JSON.stringify(newProps.PieChart[chartId]) !== JSON.stringify(this.state.mapDataFromProps) &&
      newProps.selectedTopicId.length
    ) {
      this.setState({ chartData: [] });
      const chartData =
        typeof newProps.PieChart[chartId]['data'] === 'object'
          ? this.parseDonutChart(newProps.PieChart[chartId]['data'])
          : [];
      let { mapOptions } = this.state;
      mapOptions['series'] = chartData;
      mapOptions['title']['text'] = title;
      mapOptions['caption']['text'] = `${
        newProps.PieChart[chartId]['count']
          ? newProps.PieChart[chartId]['count'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          : 0
      } <br> Comments`;
      this.setState({
        chartData,
        mapOptions,
        mapDataFromProps: newProps.PieChart[chartId],
        topicId: newProps.selectedTopicId,
        source: newProps.source,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(newProps)) {
      this.reRenderChartOnChange(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    return (
      <React.Fragment>
        {Object.keys(mapDataFromProps).length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={mapOptions} />
        )}
      </React.Fragment>
    );
  }
}
function mapStateToProps(state) {
  return {
    PieChart: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PieChart);
