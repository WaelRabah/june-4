import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import moment from 'moment';

class SpiderChart extends AbstractChart{
  constructor(props) {
    super(props);
    this.state = {
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      chartData: [],
      source: [],
      mapDataFromProps: [],
      mapOptions: {
        chart: {
          polar: true,
          type: 'area',
          backgroundColor: 'white',
          marginBottom: 0,
          marginLeft: 0,
          marginTop: 20,
          width: 370,
          height: 240,
        },
        series: [],
        legend: {
          enabled: false,
        },
        pane: {
          size: '80%',
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: { enabled: false },
        credits: {
          enabled: false,
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        xAxis: {
          tickmarkPlacement: 'on',
          lineWidth: 0,
          categories: [],
          labels: {
            style: {
              color: '#999999',
              fontSize: '10px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
          },
        },
        yAxis: {
          gridLineInterpolation: 'polygon',
          lineWidth: 0,
          tickInterval: 0.2,
          min: 0,
          labels: {
            enabled: false,
          },
          title: '',
        },
      },
    };
  }

  parseSpiderChart = chart => {
    return {
      categories: chart.categories,
      values: [
        {
          data: chart['values'],
          pointPlacement: 'on',
          color: '#2699FB',
          marker: {
            enabled: false,
          },
        },
      ],
    };
  };
  reRenderChartOnChange = props => {
    let { mapOptions } = this.state;
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        mapOptions,
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, selectedTopicId, source } = this.props;
    if (
      JSON.stringify(newProps.SpiderChart[chartId]) !==
        JSON.stringify(this.state.mapDataFromProps) &&
      newProps.selectedTopicId.length
    ) {
      this.setState({ chartData: [] });

      const { title } = this.props;
      const chartData = this.parseSpiderChart(newProps.SpiderChart[chartId]);

      let { mapOptions } = this.state;
      mapOptions['series'] = chartData['values'];
      mapOptions['xAxis']['categories'] = chartData['categories'];
      mapOptions['title']['text'] = title;
      this.setState({
        chartData,
        mapOptions,
        mapDataFromProps: newProps.SpiderChart[chartId],
        source,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(newProps)) {
      this.reRenderChartOnChange(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    return (
      <React.Fragment>
        {Object.keys(mapDataFromProps).length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={mapOptions} />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    SpiderChart: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpiderChart);
