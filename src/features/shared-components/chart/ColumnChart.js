import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsReact from 'highcharts-react-official';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import moment from 'moment';
import { chartColorCode, labelColor } from './colorCodes';

HighchartsMore(Highcharts);

class ColumnChart extends AbstractChart {
  constructor(props) {
    super(props);
    this.state = {
      mapDataFromProps: {},
      chartData: [],
      source: [],
      topicId: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          type: 'column',
          backgroundColor: 'white',
          marginBottom: 60,
          width: 370,
          height: 240,
        },
        plotOptions: {
          column: {
            dataLabels: {
              enabled: true,
            },
          },
        },
        legend: {
          enabled: false,
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: { enabled: false },
        credits: {
          enabled: false,
        },
        xAxis: {
          labels: {
            style: {
              color: '#999999',
              fontSize: '8px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
          },
        },
        yAxis: {
          labels: {
            style: {
              fontSize: '8px',
            },
            y: 2,
          },

          tickInterval: 10,
          title: '',
        },
      },
    };
  }

  parseColumnChart = chart => {
    if (Object.keys(chart).length) {
      const updatedChart = chart['word'].map((item, index) => {
        return [item.replace('_', ' '), chart['freq'][index]];
      });
      return updatedChart;
    }
  };

  reRenderChartOnChange = props => {
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, selectedTopicId, source } = this.props;
    if (
      JSON.stringify(newProps.ColumnChart[chartId]) !==
        JSON.stringify(this.state.mapDataFromProps) &&
      newProps.selectedTopicId != null
    ) {
      this.setState({ chartData: [] });
      const { title } = this.props;
      let { mapOptions } = this.state;
      const chartData = this.parseColumnChart(newProps.ColumnChart[chartId]);
      mapOptions['series'] = [
        {
          data: chartData,
          color: chartColorCode[title.split(' ')[title.split(' ').length - 2].toLowerCase()],
          dataLabels: {
            enabled: true,
            rotation: 360,
            align: 'center',
            format: '{point.y: f}',
            y: -4,
            style: {
              color: labelColor[title.split(' ')[title.split(' ').length - 2].toLowerCase()],
              fontSize: '8px',
              fontWeight: 'normal',
              textOutline: 0,
            },
          },
        },
      ];
      mapOptions['xAxis']['categories'] =
        chartData && chartData instanceof Array ? chartData.map(i => i[0]) : [];
      mapOptions['title']['text'] = this.props.title;
      this.setState({
        chartData,
        mapOptions,
        mapDataFromProps: this.props.ColumnChart[chartId],
        source,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(newProps)) {
      this.reRenderChartOnChange(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    return (
      <React.Fragment>
        {Object.keys(mapDataFromProps).length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={mapOptions} />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ColumnChart: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ColumnChart);
