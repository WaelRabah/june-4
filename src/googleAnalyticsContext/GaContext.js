import React, { Component , createContext } from 'react'
import ReactGA from 'react-ga'
export const GaContext =createContext();
export default class GaContextProvider extends Component {
    constructor(props)
    {   super(props) 
        ReactGA.initialize(process.env.REACT_APP_GOOGLE_ANALYTICS_KEY)
            
        this.state={
            ReactGA : ReactGA
        }  
    }
   
    render() {
        return (
           <GaContext.Provider value={{...this.state}}>
                {this.props.children}
           </GaContext.Provider>
        )
    }
}
