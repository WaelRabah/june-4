import React from 'react';
import { shallow } from 'enzyme';
import Feeds from '../../../../src/features/home/components/Feeds';

describe('home/components/Feeds', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const props = {
      feed: {},
      handleFeedDetails: function(){},
      updateRelevanceFeed: function(){}
    };
    const renderedComponent = shallow(<Feeds  {...props} />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});